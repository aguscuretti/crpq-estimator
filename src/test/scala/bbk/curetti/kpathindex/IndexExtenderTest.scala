package bbk.curetti.kpathindex

import bbk.crpq.pathIndex.IndexExtender
import bbk.curetti.BaseTest
import bbk.curetti.model.graph.{Edge, Node, Path, PathPrefix}
import bbk.curetti.model.stores.PathIdStore
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class IndexExtenderTest extends BaseTest {

  "IndexExtender" should {

    //TODO!!!!!!
//    "toDO: check if i insert a path for pathId that links to a path with 2 edges how many nodes I get back? all the" +
//      "ones along the path? so 3 nodes? or first and last? " in {
//
//    }


    "extend kPathIndex from k=1 to k=2" in {
      val kPathIndex = spy(new KPathIndexInMemory)
      val id1 = PathIdStore.getPathIdByEdges(List(Edge("edge1")))
      val id2 = PathIdStore.getPathIdByEdges(List(Edge("edge2")))

      val id3 = PathIdStore.getPathIdByEdges(List(Edge("edge1"), Edge("edge1")))
      val id4 = PathIdStore.getPathIdByEdges(List(Edge("edge2"), Edge("edge2")))
      val id5 = PathIdStore.getPathIdByEdges(List(Edge("edge1"), Edge("edge2")))

      val paths1 = List(Path(id1, List(Node(1), Node(2))), Path(id1, List(Node(2), Node(3))))
      val paths2 = List(Path(id2, List(Node(4), Node(5))), Path(id2, List(Node(5), Node(6))), Path(id2, List(Node(2), Node(8))))
      paths1.union(paths2).foreach(path => kPathIndex.insert(path))

      IndexExtender.run(kPathIndex, 2)
      there was one(kPathIndex).insert(Path(id1, List(Node(1), Node(2))))
      there was one(kPathIndex).insert(Path(id1, List(Node(2), Node(3))))
      there was one(kPathIndex).insert(Path(id2, List(Node(4), Node(5))))
      there was one(kPathIndex).insert(Path(id2, List(Node(5), Node(6))))
      there was one(kPathIndex).insert(Path(id2, List(Node(2), Node(8))))
      there was one(kPathIndex).insert(Path(id3, List(Node(1), Node(2), Node(3))))
      there was one(kPathIndex).insert(Path(id4, List(Node(4), Node(5), Node(6))))
      there was one(kPathIndex).insert(Path(id5, List(Node(1), Node(2), Node(8))))

    }

    "extend index to k=3" in {
      val kPathIndex = spy(new KPathIndexInMemory)
      val id1 = PathIdStore.getPathIdByEdges(List(Edge("edge1")))
      val id2 = PathIdStore.getPathIdByEdges(List(Edge("edge1"), Edge("edge1")))
      val id3 = PathIdStore.getPathIdByEdges(List(Edge("edge1"), Edge("edge1"), Edge("edge1")))

      val paths1 = List(
        Path(id1, List(Node(1), Node(2))),
        Path(id1, List(Node(2), Node(3))),
        Path(id1, List(Node(3), Node(4))))

      paths1.foreach(path => kPathIndex.insert(path))

      IndexExtender.run(kPathIndex, 3)
      there was one(kPathIndex).insert(Path(id1, List(Node(1), Node(2))))
      there was one(kPathIndex).insert(Path(id1, List(Node(2), Node(3))))
      there was one(kPathIndex).insert(Path(id1, List(Node(3), Node(4))))
      there was one(kPathIndex).insert(Path(id2, List(Node(1), Node(2), Node(3))))
      there was one(kPathIndex).insert(Path(id3, List(Node(1), Node(2), Node(3), Node(4))))
    }

    "insert same path only once" in {
      val kPathIndex = spy(new KPathIndexInMemory)
      val id1: Long = PathIdStore.getPathIdByEdges(List(Edge("edge1")))
      val id2: Long = PathIdStore.getPathIdByEdges(List(Edge("edge2")))

      val paths1 = List(Path(id1, List(Node(1), Node(2))), Path(id1, List(Node(1), Node(2))))
      val paths2 = List(Path(id2, List(Node(2), Node(3))))
      paths1.union(paths2).foreach(path => kPathIndex.insert(path))

      IndexExtender.run(kPathIndex, 1)
      there were two(kPathIndex).insert(Path(id1, List(Node(1), Node(2))))
      there was one(kPathIndex).insert(Path(id2, List(Node(2), Node(3))))

      val pathsId1 = kPathIndex.search(PathPrefix(id1, List.empty))
      pathsId1.size must beEqualTo(1)
      val pathsId2 = kPathIndex.search(PathPrefix(id2, List.empty))
      pathsId2.size must beEqualTo(1)
    }
  }

}
