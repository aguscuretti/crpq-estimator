package bbk.curetti.kpathindex;

import java.nio.file.Files

import bbk.curetti.BaseTest
import bbk.curetti.model.graph.{Node, Path, PathPrefix}
import org.apache.commons.io.FileUtils
import org.junit.runner.RunWith
import org.specs2.mutable.After
import org.specs2.runner.JUnitRunner
import org.specs2.specification.Scope

@RunWith(classOf[JUnitRunner])
class KPathIndexInMemoryTest extends BaseTest {

    "index" should {
        "insert a path" in new TestEnv {
            val path = Path(0, List(Node(1), Node(3), Node(5)))
            index.insert(path)
            val result = index.search(PathPrefix(0))
            result.head must beEqualTo(path)
            result.size must beEqualTo(1)
        }

        "search a path given pathPrefix and source node" in new TestEnv {
            val paths = List(
                Path(1, List(Node(1), Node(2), Node(3))),
                Path(1, List(Node(1), Node(12), Node(13)))
            )
            paths.foreach(path => index.insert(path))

            val result = index.search(PathPrefix(1, List(Node(1))))

            result must containTheSameElementsAs(paths)
        }

        "search a path by path prefix only" in new TestEnv {
            val path1 = Path(1, List(Node(1), Node(2), Node(3)))
            val path2 = Path(1, List(Node(1), Node(12), Node(13)))
            val path3 = Path(2, List(Node(1), Node(12), Node(13)))
            val paths = List(path1, path2, path3)
            paths.foreach(path => index.insert(path))

            val result = index.search(PathPrefix(1, List.empty))
            val expected = List(path1, path2)
            result must containTheSameElementsAs(expected)
        }

    }

    trait TestEnv extends Scope with After {
        val indexTmpPath = Files.createTempDirectory("PathIndex").toFile.getAbsoluteFile
        val index = new KPathIndexInMemory()


        override def after: Unit = FileUtils.deleteQuietly(indexTmpPath)
    }

    trait TestPath extends Scope {

        def createPath(id: Long): Path = {
            return Path(id, List())
        }
    }
}