package bbk.curetti.kpathindex

import bbk.curetti.BaseTest
import bbk.curetti.model.graph.PathPrefix
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FileImporterTest extends BaseTest {

  "FileImporter" should {
    "import all lines in dataset file" in {
      val kPathIndex = new KPathIndexInMemory()
      val labels = List("6", "8", "1")

      val result = FileImporter.buildBaseIndex(kPathIndex, "advogato", labels)
      result must beEqualTo(18)

      val pathsWithId1 = kPathIndex.search(PathPrefix(1, List.empty))
      val totalPaths1 = pathsWithId1.iterator.count(_ => true)
      totalPaths1 must beEqualTo(5)

      val pathsWithId2 = kPathIndex.search(PathPrefix(2, List.empty))
      val totalPaths2 = pathsWithId2.iterator.count(_ => true)
      totalPaths2 must beEqualTo(8)

      val pathsWithId3 = kPathIndex.search(PathPrefix(3, List.empty))
      val totalPaths3 = pathsWithId3.iterator.count(_ => true)
      totalPaths3 must beEqualTo(5)
    }

  }
}
