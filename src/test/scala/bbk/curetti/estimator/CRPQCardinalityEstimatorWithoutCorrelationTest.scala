//package bbk.curetti.estimator
//
//import bbk.curetti.kpathtable.{KPathTableInMemory, PathIdStats}
//import bbk.curetti.BaseTest
//import bbk.curetti.kpathindex.KPathIndexInMemory
//import bbk.curetti.model.query.{CRPQ, QueryNode, RPQ}
//import org.junit.runner.RunWith
//import org.specs2.runner.JUnitRunner
//import org.specs2.specification.Scope
//
//import scala.collection.mutable.ListBuffer
//
//
//@RunWith(classOf[JUnitRunner])
//class CRPQCardinalityEstimatorWithoutCorrelationTest extends BaseTest {
//
//  "CRPQ Cardinality Estimator without Correlation" should {
//
//    "get unique nodes from a CRPQ" in new KPathTableEnv {
//      val nodes = estimator.getNodeIdsFromCrpq(crpq)
//      nodes must containTheSameElementsAs(Seq("a", "b", "c"))
//    }
//
//    "return empty incoming path ids for a node withouth any incoming edges" in new KPathTableEnv {
//      val incomingIds = estimator.getIncomingPathIdsFor("a", crpq)
//      incomingIds must containTheSameElementsAs(Seq())
//    }
//
//    "return incoming path ids for a node with incoming edges" in new KPathTableEnv {
//      val incomingIds = estimator.getIncomingPathIdsFor("b", crpq)
//      incomingIds.head must beEqualTo((1,"a"))
//    }
//
//    "return outgoing path ids for a node with outgoing edges" in new KPathTableEnv {
//      val outgoingIds = estimator.getOutgoingPathIdsFor("b", crpq)
//      outgoingIds.head must beEqualTo((2,"c"))
//    }
//
//    "return incoming path ids for a node with outgoing edges" in new KPathTableEnv {
//      val incomingIds = estimator.getIncomingPathIdsFor(node = "c", crpq = crpq, avoidConnectiosToNodes = Some(List("a")))
//      incomingIds.head must beEqualTo((2, "b"))
//    }
//
//    "intersect possible values sets" in new KPathTableEnv {
//      val possibleValues = ListBuffer[Set[Long]]()
//      possibleValues += Set(1, 2)
//      possibleValues += Set(1, 2, 3, 4, 5)
//      val intersectionResult = estimator.intersectPossibleValuesSets(possibleValues)
//      intersectionResult.toSeq must containTheSameElementsAs(Seq(1, 2))
//    }
//
//    "calculate result for a node of the crpq" in new KPathTableEnv {
//      val incomingPathsForA = estimator.getIncomingPathIdsFor("a", crpq)
//      val outgoingPathsForA = estimator.getOutgoingPathIdsFor("a", crpq)
//      val resultForA = estimator.getNodeValues(incomingPathsForA, outgoingPathsForA)
//      // result should be intersection of sources from k-path table for pathId 1 and 3 == Set(1,2)
//      resultForA.size should be equalTo(2)
//      resultForA should containTheSameElementsAs(Seq(1, 2))
//    }
//
//    "calculate result for b node of the crpq" in new KPathTableEnv {
//      val incomingPathsForB = estimator.getIncomingPathIdsFor("b", crpq)
//      val outgoingPathsForB = estimator.getOutgoingPathIdsFor("b", crpq)
//      val resultForB = estimator.getNodeValues(incomingPathsForB, outgoingPathsForB)
//      // result should be intersection of targets from k-path table for pathId 1 and sources for pathId 3 == Set(4,5)
//      resultForB.size should be equalTo(2)
//    }
//
//    "calculate result for c node of the crpq" in new KPathTableEnv {
//      val incomingPathsForC = estimator.getIncomingPathIdsFor("c", crpq)
//      val outgoingPathsForC = estimator.getOutgoingPathIdsFor("c", crpq)
//      val resultForC = estimator.getNodeValues(incomingPathsForC, outgoingPathsForC)
//      // result should be intersection of targets from k-path table for pathId 2 and 3 == Set(13)
//      resultForC.size should be equalTo(1)
//    }
//
//    "estimate CRPQ cardinality by multiplying the intersection result for each node in the CRPQ" in new KPathTableEnv {
//      // multiplying above partial results for each node: 2 * 2 * 1 = 4
//      val estimatedCardinality = estimator.estimateCardinality(crpq)
//      estimatedCardinality should be equalTo(4)
//    }
//  }
//
//  trait KPathTableEnv extends Scope {
//    val kPathTable = new KPathTableInMemory()
//    kPathTable.insert(1, PathIdStats(sources = Set(1,2,3), targets = Set(4,5,6,7)))
//    kPathTable.insert(2, PathIdStats(sources = Set(4,5,10), targets = Set(13, 25, 30)))
//    kPathTable.insert(3, PathIdStats(sources = Set(1,2,7), targets = Set(13, 15, 17)))
//
//    val kPathIndex = new KPathIndexInMemory()
//    val samplingFactor = 1 // do not change for tests
//    val estimator = new CRPQCardinalityEstimatorBernoulli(kPathTable, kPathIndex, samplingFactor)
//
//    val rpq1 = RPQ(QueryNode("a"), QueryNode("b"), 1)
//    val rpq2 = RPQ(QueryNode("b"), QueryNode("c"), 2)
//    val rpq3 = RPQ(QueryNode("a"), QueryNode("c"), 3)
//    val crpq = CRPQ(List(rpq1, rpq2, rpq3))
//  }
//
//}
//
//
