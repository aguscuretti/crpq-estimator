//package bbk.curetti.estimator
//
//import bbk.curetti.BaseTest
//import bbk.curetti.kpathindex.KPathIndexInMemory
//import bbk.curetti.kpathtable.{KPathTableInMemory, PathIdStats}
//import bbk.curetti.model.graph.{Edge, Node, Path}
//import bbk.curetti.model.query.{CRPQ, QueryNode, RPQ}
//import bbk.curetti.model.stores.PathIdStore
//import bbk.curetti.sampling.NodeSampler
//import org.junit.runner.RunWith
//import org.specs2.runner.JUnitRunner
//import org.specs2.specification.Scope
//
//
//@RunWith(classOf[JUnitRunner])
//class CRPQCardinalityEstimatorWithCorrelationTest extends BaseTest {
//
//  "CRPQ Estimator With Correlation" should {
//
//    "calculate correlated result for a node of the crpq" in new TestEnvironment {
//      val incomingPathsForA = correlatedEstimator.getIncomingPathIdsFor("a", crpq) // should be empty
//      val outgoingPathsForA = correlatedEstimator.getOutgoingPathIdsFor("a", crpq) // should be path ids 1 and 3
//      incomingPathsForA should beEmpty
//      outgoingPathsForA should containTheSameElementsAs(Seq((1, "b"), (3, "c")))
//
//      val resultForA = correlatedEstimator.getNodeValues(incomingPathsForA, outgoingPathsForA)
//      resultForA.size should be equalTo(1)
//      resultForA should containTheSameElementsAs(Seq(2))
//    }
//
//    "calculate correlated result for b node of the crpq" in new TestEnvironment {
//      val incomingPathsForB = correlatedEstimator.getIncomingPathIdsFor("b", crpq)
//      val outgoingPathsForB = correlatedEstimator.getOutgoingPathIdsFor("b", crpq)
//      incomingPathsForB should containTheSameElementsAs(Seq((1, "a")))
//      outgoingPathsForB should containTheSameElementsAs(Seq((2, "c")))
//
//      val resultForB = correlatedEstimator.getNodeValues(incomingPathsForB, outgoingPathsForB)
//      // result should be
//      resultForB.size should be equalTo(2)
//      resultForB should containTheSameElementsAs(Seq(3,4))
//    }
//
//    "calculate result for c node of the crpq" in new TestEnvironment {
//      val incomingPathsForC = correlatedEstimator.getIncomingPathIdsFor("c", crpq)
//      val outgoingPathsForC = correlatedEstimator.getOutgoingPathIdsFor("c", crpq)
//      incomingPathsForC should containTheSameElementsAs(Seq((2, "b"), (3, "a")))
//      outgoingPathsForC should beEmpty
//
//      val resultForC = correlatedEstimator.getNodeValues(incomingPathsForC, outgoingPathsForC)
//      // result should be
//      resultForC should containTheSameElementsAs(Seq(6,7))
//    }
//
//    "estimate CRPQ cardinality when initial node is a" in new TestEnvironment {
//      nodeSampler.takeSample[String](Seq(any[String], any[String], any[String]), 1) returns Seq("a")
//      val estimatedCardinality = correlatedEstimator.estimateCardinality(crpq, ScaleUpStrategies.none)  //correlatedEstimator.estimateCardinality(crpq, ScaleUpStrategies.scaleWithSf)
//      // starting from a = 2 -> nodePossibleValues: Map(b -> Set(3), a -> Set(2), c -> Set(6, 7))
//      estimatedCardinality should beEqualTo(2)
//
//    }
//
//    "estimate CRPQ cardinality when initial node is b" in new TestEnvironment {
//      nodeSampler.takeSample[String](Seq(any[String], any[String], any[String]), 1) returns Seq("b")
//      val estimatedCardinality = correlatedEstimator.estimateCardinality(crpq, ScaleUpStrategies.none)
//
//      // starting from b=3 -> final nodePossibleValues: Map(b -> Set(3), a -> Set(2), c -> Set(6, 7)) => * sets size = 1*1*2=2
//      // starting from b=4 -> final nodePossibleValues: Map(b -> Set(4), a -> Set(), c -> Set())  => * sets size = 1*0*0=0
//      // final 2 + 0 = 2
//      estimatedCardinality should beEqualTo(2)
//    }
//
//    "estimate CRPQ cardinality when initial node is c" in new TestEnvironment {
//      nodeSampler.takeSample[String](Seq(any[String], any[String], any[String]), 1) returns Seq("c")
//      val estimatedCardinality = correlatedEstimator.estimateCardinality(crpq, ScaleUpStrategies.none)
//      // starting from c = 6 -> final nodePossibleValues: Map(b -> Set(3), a -> Set(2), c -> Set(6)) => 1 * 1 *1 = 1
//      // starting from c = 7 -> final nodePossibleValues: Map(b -> Set(3), a -> Set(2), c -> Set(7)) => 1 * 1 *1 = 1
//      // 1 + 1 = 2
//      estimatedCardinality should beEqualTo(2)
//    }
//
//    "estimate CRPQ where initial node has 2 possible values. check map is reset" in new TestEnvironment {
//
//    }
//
//    "get total answer paths for a crpq" in new TestEnvironment {
//      val possibleValuesForEachNode = collection.mutable.Map[String, Set[Long]]()
//      possibleValuesForEachNode.update("a", Set(2))
//      possibleValuesForEachNode.update("b", Set(3))
//      possibleValuesForEachNode.update("c", Set(6,7))
//      val totalPaths = correlatedEstimator.getTotalPathsForCRPQ(crpq, possibleValuesForEachNode.toMap)
//      totalPaths must beEqualTo(5)
//    }
//  }
//
//  trait TestEnvironment extends Scope {
//    val kPathIndex = new KPathIndexInMemory()
//    val labels = List("6", "8", "1")
//    val id1 = PathIdStore.getPathIdByEdges(List(Edge("6")))
//    val id2 = PathIdStore.getPathIdByEdges(List(Edge("8")))
//    val id3 = PathIdStore.getPathIdByEdges(List(Edge("1")))
//    kPathIndex.insert(Path(id1, List(Node(2), Node(3))))
//    kPathIndex.insert(Path(id1, List(Node(2), Node(4))))
//    kPathIndex.insert(Path(id1, List(Node(2), Node(5))))
//
//    kPathIndex.insert(Path(id2, List(Node(3), Node(6))))
//    kPathIndex.insert(Path(id2, List(Node(3), Node(7))))
//    kPathIndex.insert(Path(id2, List(Node(4), Node(8))))
//    kPathIndex.insert(Path(id2, List(Node(4), Node(9))))
//
//    kPathIndex.insert(Path(id3, List(Node(2), Node(6))))
//    kPathIndex.insert(Path(id3, List(Node(2), Node(7))))
//    kPathIndex.insert(Path(id3, List(Node(10), Node(6))))
//    kPathIndex.insert(Path(id3, List(Node(10), Node(7))))
//
//    val samplingFactor = 1 // do not change for tests
//    val nodeSampler = smartMock[NodeSampler]
//
//    val kPathTable = new KPathTableInMemory()
//    kPathTable.insert(1, PathIdStats(sources = Set(2), targets = Set(3,4,5)))
//    kPathTable.insert(2, PathIdStats(sources = Set(3,4), targets = Set(6,7,8,9)))
//    kPathTable.insert(3, PathIdStats(sources = Set(2,10), targets = Set(6,7)))
//
//    val correlatedEstimator = new CRPQCardinalityEstimatorWithCorrelation(kPathTable, kPathIndex, samplingFactor)
//
//    val rpq1 = RPQ(QueryNode("a"), QueryNode("b"), 1)
//    val rpq2 = RPQ(QueryNode("b"), QueryNode("c"), 2)
//    val rpq3 = RPQ(QueryNode("a"), QueryNode("c"), 3)
//    val crpq = CRPQ(List(rpq1, rpq2, rpq3))
//  }
//}
