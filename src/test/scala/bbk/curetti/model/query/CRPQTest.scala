package bbk.curetti.model.query

import bbk.curetti.BaseTest
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CRPQTest extends BaseTest{

  "Query" should {

    "print latex version of the crpq" in {
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 6)
      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 8)
      val testCrpq = CRPQ(List(rpq1, rpq2))

      val result = CRPQ.latexPrint(testCrpq)

      val expected = """\begin{tabular}[c]{@{}c@{}} x $\xrightarrow{\text{6}}$ y \\ y $\xrightarrow{\text{8}}$ z \end{tabular}"""

      result should beTheSameAs(expected)
    }

  }
}


