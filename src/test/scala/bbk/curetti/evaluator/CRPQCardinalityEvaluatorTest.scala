package bbk.curetti.evaluator

import bbk.curetti.BaseTest
import bbk.curetti.evaluator.evaluator.CRPQCardinalityEvaluator
import bbk.curetti.kpathindex.KPathIndexInMemory
import bbk.curetti.model.graph.{Edge, Node, Path}
import bbk.curetti.model.query.{CRPQ, QueryNode, RPQ}
import bbk.curetti.model.stores.PathIdStore
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.specs2.specification.Scope

import scala.collection.mutable.ListBuffer

@RunWith(classOf[JUnitRunner])
class CRPQCardinalityEvaluatorTest extends BaseTest {

  "CRPQCardinalityEstimator" should {


    "sort individual rpqs within a CRPQ in ASC order of number of paths" in new TestEnvironment {
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1) // has 6 paths
      val rpq2 = RPQ(QueryNode("x"), QueryNode("z"), 2) // has 8 paths
      val rpqs = List(rpq2, rpq1)
      val result = estimator.sortByCardinality(rpqs)
      result must beEqualTo(List(rpq1, rpq2))
    }

    "estimate list of only 1 RPQ: independent case" in new TestEnvironment {
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1) // has 6 paths
      val rpqs = List(rpq1)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))
      result must beEqualTo(6)
    }

    "process consecutive RPQs with same target" in new TestEnvironment {
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1) // has 6 paths
      val rpq2 = RPQ(QueryNode("z"), QueryNode("y"), 2) // has 8 paths
      val rpqs = List(rpq1, rpq2)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))
      // x       1        1      5
      //    y    *   3    *  2   *   6
      // z      2,6       6     2,4
      // _______________________________
      //         2   +    1   +   2   = 5
      result must beEqualTo(5)
    }

    "process consecutive RPQs with are concatenated" in new TestEnvironment {
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1) // has 6 paths
      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2) // has 8 paths
      val rpqs = List(rpq1, rpq2)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))
      // x   -> y ->  z
      //  1  -> 2 ->  3,6     1 * 1 * 2 = 2
      // 1,6 -> 4 ->  6,7     2 * 1 * 2 = 4
      //   5 -> 6 ->  2,3,7   1 * 1 * 3 = 3
      //                   _________________
      //                            sum = 9
      result must beEqualTo(9)
    }

    "process consecutive RPQs with are concatenated inverse" in new TestEnvironment {
      val rpq1 = RPQ(QueryNode("y"), QueryNode("z"), 1) // has 6 paths 
      val rpq2 = RPQ(QueryNode("x"), QueryNode("y"), 2) // has 8 paths
      val rpqs = List(rpq1, rpq2)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))
      //    x -[2]-> y    y -[1]-> z    RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      //    4 -> 6, 7     5 -> 6           =>   4,2,6  -> 6 ->  2,3,7           1*3= 3
      //    2 -> 3,6      1 -> 2, 3, 4
      //    5  -> 7       9 -> 10
      //    6 -> 2,3,7    6 -> 4                                               SUM=9

      result must beEqualTo(9)
    }



    "process consecutive RPQs with same source" in new TestEnvironment {
      //   x -> y       x -> z
      //   5 -> 6       5 -> 7
      //   6 -> 4       6 -> 2, 3, 7
      //
      //   RESULTS:
      //   multiply nodes per target:  to y we have 2
      //
      //
      //

      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1) // has 6 paths
      val rpq2 = RPQ(QueryNode("x"), QueryNode("z"), 2) // has 8 paths
      val rpqs = List(rpq2, rpq1)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))
      result must beEqualTo(4)
    }

    "process RPQs when values for nodes being concatenated already have possible values" in new TestEnvironment {
      //  x -id:3-> y     p -id:1-> a       y-id:2->p                                     x- > y -> p -> a
      //  2 ->  5         1  ->  2,3,4      4 -> 6, 7   --> we have y=4 and p=6  => KEEP  3 -> 4 -> 6 -> 4
      //  2 ->  7         5  ->  6          2 -> 3,6    --> no y=2
      //  3 ->  4         6  ->  4          5  -> 7     --> yes y=5 but no p=7
      //  3 ->  6         9  ->  10         6 -> 2,3,7  --> yes y=6 but no p=2,3,7
      //  4 ->  3
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 3) // has 5 paths
      val rpq2 = RPQ(QueryNode("p"), QueryNode("a"), 1) // has 6 paths
      val rpq3 = RPQ(QueryNode("y"), QueryNode("p"), 2) // has 8 paths
      val rpqs = List(rpq1, rpq2, rpq3)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      // x- > y -> p -> a
      // 3 -> 4 -> 6 -> 4
      result must beEqualTo(1)
    }

    "process RPQs length 3 that end on same target" in new TestEnvironment {
      //  x -[1]-> y     y -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
      // 1 -> 2, 3, 4     2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9
      //
      //  p -(3)-> a -(1)-> z         RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      //  2 ->  5,7     1  ->  2,3,4               =>      2 -> 5 -> 6   = 1
      //  3 ->  4,6     5  ->  6                           3 -> 6 -> 4   = 1
      //  4 ->  3       6  ->  4                                          SUM=2
      //
      // JOIN INDIVIUDAL RPQ RESULTS BY JOINING VALUE ON NODE 'Z'
      //  From 1st rpq values for Z=2,3,6,7
      //  From 2nd rpq values for Z=4,6
      //  In common only Z=6, so possible results are:
      //     1)      1  -> 2 -> 6     combined with   2 -> 5 -> 6
      //            1,6 -> 4 -> 6
      //     SO:         3                *               1           = 3
      //                                                              RESULT=3
      //
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
      val rpq3 = RPQ(QueryNode("p"), QueryNode("a"), 3)
      val rpq4 = RPQ(QueryNode("a"), QueryNode("z"), 1)
      val rpqs = List(rpq1, rpq2, rpq3, rpq4)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      // x -[1]-> y -[2]-> z
      // p -(3)-> a -(1)-> z
      result must beEqualTo(3)
    }

    "process RPQs length 3 that are joined on middle node" in new TestEnvironment {
      //  x -[1]-> y     y -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
      // 1 -> 2, 3, 4     2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9
      //
      //  a -(2)-> y -(1)-> b        RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      //   4 -> 6, 7       5 -> 6                     =>   4 ,2  -> 6 -> 4   =      2*1 = 2
      //   2 -> 3,6        1 -> 2, 3, 4
      //   5  -> 7         9 -> 10                                           SUM=   2
      //   6 -> 2,3,7      6 -> 4
      //
      // JOIN INDIVIUDAL RPQ RESULTS BY JOINING VALUE ON NODE 'Y'
      //  From 1st rpq values for Y=2,4,6
      //  From 2nd rpq values for X=6
      //  In common only Y=6, so possible results are:
      //     1)      4 ,2  -> 6 -> 4     combined with   5  -> 6 ->  2,3,7
      //
      //     SO:         2 *1 = 2                *         1*3= 3              = 6
      //                                                              RESULT=6
      //
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
      val rpq3 = RPQ(QueryNode("a"), QueryNode("y"), 2)
      val rpq4 = RPQ(QueryNode("y"), QueryNode("b"), 1)
      val rpqs = List(rpq1, rpq2, rpq3, rpq4)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      // x -[1]-> y -[2]-> z
      // a -(3)-> y -(1)-> b
      result must beEqualTo(6)
    }


    "QueryResult should " in {
      val results = ListBuffer[RPQResult]()
      results += RPQResult("t", "x", 2, List(5))
      results += RPQResult("t", "x", 3, List(6))
      val qr = new QueryResult
      val listTargetIncoming = qr.mapNumberOfSourcesToEachTargetIn(results, List("t"))
      listTargetIncoming must containTheSameElementsAs(List((5,1),(6,1)))
    }

    "QueryResult should " in {
      val results = ListBuffer[RPQResult]()
      results += RPQResult("t", "x", 2, List(5, 3))
      results += RPQResult("t", "x", 3, List(6, 3))
      val qr = new QueryResult
      val listTargetIncoming = qr.mapNumberOfSourcesToEachTargetIn(results, List("t"))
      listTargetIncoming must containTheSameElementsAs(List((5,1), (3,2),(6,1)))
    }

    "length 3: all concatenated" in new TestEnvironment {
      //                                        Result of concatenation:
      //  t -(3)-> x      x -[1]-> y            t     x    y
      //  2 ->  5,7      5 -> 6                 2 ->  5 -> 6
      //  3 ->  4,6      1 -> 2, 3, 4           3 ->  6 -> 4
      //  4 ->  3        9 -> 10
      //                 6 -> 4
      //
      //   merge this:   with:
      //   t    x    y    y -[2]-> z               t     x    y     z
      //  2 ->  5 -> 6    4 -> 6, 7                2 ->  5 -> 6 -> 2,3,7
      //  3 ->  6 -> 4    2 -> 3,6                 3 ->  6 -> 4 -> 6, 7
      //                  5  -> 7
      //                  6 -> 2,3,7
      //

      val rpq1 = RPQ(QueryNode("t"), QueryNode("x"), 3)
      val rpq2 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq3 = RPQ(QueryNode("y"), QueryNode("z"), 2)

      val rpqs = List(rpq1, rpq2, rpq3)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      // final results:
      // 2 -> 5 -> 6 -> 2,3,7 ==> 3 possibilities
      // 3 -> 6 -> 4 -> 6,7 ==> 2 possibilities
      result must beEqualTo(5)
    }

    "length 4: all concatenated" in new TestEnvironment {
      //  t -(3)-> x   z -[3]> a    x -[1]-> y     y -[2]-> z     (HERE LOOKING FOR DEST VALUES Z=2,3,4 and Join values Y=4,6)
      //  2 ->  5,7    2 ->  5,7   5 -> 6          6 -> 2,3             so we end up having:  t     x    y     z    a
      //  3 ->  4,6    3 ->  4,6   6 -> 4                                                     2 ->  5 -> 6 ->  2 -> 5,7
      //  4 ->  3      4 ->  3                                                                2 ->  5 -> 6 ->  3 ->  4,6
      //
      val rpq1 = RPQ(QueryNode("t"), QueryNode("x"), 3)
      val rpq2 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq3 = RPQ(QueryNode("y"), QueryNode("z"), 2)
      val rpq4 = RPQ(QueryNode("z"), QueryNode("a"), 3)

      val rpqs = List(rpq1, rpq2, rpq3, rpq4)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      // final results:
      // 2  -> 5 ->  6 -> 3 -> 4,6
      //               -> 2 -> 5,7
      result must beEqualTo(4)
    }

    "length 4: same source" in new TestEnvironment {
      //  t -(3)-> x   z -[3]> a    x -[1]-> y     y -[2]-> z     (HERE LOOKING FOR DEST VALUES Z=2,3,4 and Join values Y=4,6)
      //  2 ->  5,7    2 ->  5,7   5 -> 6          6 -> 2,3             so we end up having:  t     x    y     z    a
      //  3 ->  4,6    3 ->  4,6   6 -> 4                                                     2 ->  5 -> 6 ->  2 -> 5,7
      //  4 ->  3      4 ->  3                                                                2 ->  5 -> 6 ->  3 ->  4,6
      //
      //  t -(3)-> r   r -[3]> s    s -[1]-> u     u -[2]-> v     (HERE LOOKING FOR DEST VALUES Z=2,3,4 and Join values Y=4,6)
      //  2 ->  5,7    2 ->  5,7   5 -> 6          6 -> 2,3             so we end up having:  t     r    s     u    v
      //  3 ->  4,6    3 ->  4,6   6 -> 4                                                     2 ->  5 -> 6 ->  2 -> 5,7
      //  4 ->  3      4 ->  3                                                                2 ->  5 -> 6 ->  3 ->  4,6
      //
      val rpq1 = RPQ(QueryNode("t"), QueryNode("x"), 3)
      val rpq2 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq3 = RPQ(QueryNode("y"), QueryNode("z"), 2)
      val rpq4 = RPQ(QueryNode("z"), QueryNode("a"), 3)

      val rpq5 = RPQ(QueryNode("t"), QueryNode("r"), 3)
      val rpq6 = RPQ(QueryNode("r"), QueryNode("s"), 1)
      val rpq7 = RPQ(QueryNode("s"), QueryNode("u"), 2)
      val rpq8 = RPQ(QueryNode("u"), QueryNode("v"), 3)

      val rpqs = List(rpq1, rpq2, rpq3, rpq4, rpq5, rpq6, rpq7, rpq8)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      // final results:
      // 2  -> 5 ->  6 -> 3 -> 4,6
      //  \           \ -> 2 -> 5,7
      //    -> 5 ->  6 -> 3 -> 4,6
      //              \ -> 2 -> 5,7

      result must beEqualTo(16)
    }

    "length 1" in new TestEnvironment {
      //  t -(3)-> x   z -[3]> a    x -[1]-> y     y -[2]-> z     (HERE LOOKING FOR DEST VALUES Z=2,3,4 and Join values Y=4,6)
      //  2 ->  5,7    2 ->  5,7   5 -> 6          6 -> 2,3             so we end up having:  t     x    y     z    a
      //  3 ->  4,6    3 ->  4,6   6 -> 4                                                     2 ->  5 -> 6 ->  2 -> 5,7
      //  4 ->  3      4 ->  3                                                                2 ->  5 -> 6 ->  3 ->  4,6
      //
      //  t -(3)-> r   r -[3]> s    s -[1]-> u     u -[2]-> v     (HERE LOOKING FOR DEST VALUES Z=2,3,4 and Join values Y=4,6)
      //  2 ->  5,7    2 ->  5,7   5 -> 6          6 -> 2,3             so we end up having:  t     r    s     u    v
      //  3 ->  4,6    3 ->  4,6   6 -> 4                                                     2 ->  5 -> 6 ->  2 -> 5,7
      //  4 ->  3      4 ->  3                                                                2 ->  5 -> 6 ->  3 ->  4,6
      //
      val rpq1 = RPQ(QueryNode("t"), QueryNode("x"), 3)
      val rpq5 = RPQ(QueryNode("t"), QueryNode("r"), 3)

      val rpqs = List(rpq1, rpq5)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      // final results: 2*2 + 2*2 + 1*1 = 9
      result must beEqualTo(9)
    }

//    //TODO: fix
//    "length 4: same source another one" in new IndexEnv {
//      //  t -(3)-> x      x -[1]-> y     y -[2]-> z    z -[3]> a   (HERE LOOKING FOR DEST VALUES Z=2,3,4 and Join values Y=4,6)
//      //  2 ->  5,7       5 -> 6          6 -> 2,3      2 ->  5,7         so we end up having:  t     x    y     z    a
//      //  3 ->  4,6                                     3 -> 4,6                                2 ->  5 -> 6 ->  2 -> 5,7
//      //  4 ->  3                                                                               2 ->  5 -> 6 ->  3 -> 4,6
//      //
//      //  t -(3)-> r
//      //  2 ->  5,7
//      //  3 ->  4,6
//      //  4 ->  3
//      //
//      val rpq1 = RPQ(QueryNode("t"), QueryNode("x"), 3)
//      val rpq2 = RPQ(QueryNode("x"), QueryNode("y"), 1)
//      val rpq3 = RPQ(QueryNode("y"), QueryNode("z"), 2)
//      val rpq4 = RPQ(QueryNode("z"), QueryNode("a"), 3)
//
//      val rpq5 = RPQ(QueryNode("t"), QueryNode("r"), 3)
//
//      val rpqs = List(rpq1, rpq2, rpq3, rpq4, rpq5)
//      val result = estimator.evaluateCardinality(CRPQ(rpqs))
//
//      // final results:   (z brakes into 2 paths
//      // t  -> x ->  y -> z -> a        t  -> x ->  y -> z -> a
//      // 2  -> 5 ->  6 -> (3) -> 4,6      2  -> 5 ->  6  -> (2) -> 5,7
//      //   \   r                           \    r
//      //    -> 5,7                          -> 5,7
//      //          2 * 2              +    2 * 2                    = 8
//      result must beEqualTo(8)
//    }

    "length 2: same target node" in new TestEnvironment {
      //  x -[1]-> y     y -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
      // 1 -> 2, 3, 4  r   2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9
      //
      //  p -[1]-> a XX     a -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
      // 1 -> 2, 3, 4     2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9

      // JOIN INDIVIUDAL RPQ RESULTS BY JOINING VALUE ON NODE 'X'

      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
//      val rpq3 = RPQ(QueryNode("p"), QueryNode("a"), 1)
      val rpq4 = RPQ(QueryNode("a"), QueryNode("z"), 2)
      val rpqs = List(rpq1, rpq2, rpq4)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      result must beEqualTo(15)
    }

    "length 3: concatenated disordered" in new TestEnvironment {
      //  x -[1]-> y     y -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:                 x
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3                                     2 -> 5  -> y: 6 -> z: 2,3,7    |\ 1 -> [5] -> 3*3  = 1 * 9 = 9
      // 1 -> 2, 3, 4  r   2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2                                            -> a: 6 -> b: 2,3,7    |/                                 + = 13
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4                                     3 -> 6  -> y: 4 -> z: 6,7      |\ 1 -> [6] -> 2*2  =  1 * 4 = 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9                                           -> a: 4 -> b: 6,7      |/
      //
      //  x -[1]-> a     a -[2]-> b     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
      // 1 -> 2, 3, 4     2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9

      //  t -(3)-> x    ==> Joining values: 5,6    Pre 5 possibilities: 1 (value=2)
      //  2 ->  5,7                                Pre 6 possibilities: 1 (value=3)
      //  3 ->  4,6
      //  4 ->  3

      // JOIN INDIVIUDAL RPQ RESULTS BY JOINING VALUE ON NODE 'X'
      //  From 1st rpq values for X = 5, 6
      //  Joining value 5: In: 1 Out final: 3
      //  Joining value 6: In: 1 Out final: 2
      //      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      //      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
      val rpq3 = RPQ(QueryNode("x"), QueryNode("a"), 1)
      val rpq4 = RPQ(QueryNode("a"), QueryNode("b"), 2)
      val rpq5 = RPQ(QueryNode("t"), QueryNode("x"), 3)
      val rpqs = List(rpq3, rpq4, rpq5)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      result must beEqualTo(5)
    }

    "same source: 1 branch length2, the other branch length 1" in new TestEnvironment {
      //  x -[1]-> y     y -[2]-> z
      // 5 -> 6           4 -> 6, 7
      // 1 -> 2, 3, 4     2 -> 3,6
      // 9 -> 10          5  -> 7
      // 6 -> 4           6 -> 2,3,7
      //
      //  x -[1]-> a
      // 5 -> 6
      // 1 -> 2, 3, 4
      // 9 -> 10
      // 6 -> 4

      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
      val rpq3 = RPQ(QueryNode("x"), QueryNode("a"), 1)
      val rpqs = List(rpq1, rpq2, rpq3)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      result must beEqualTo(17)
    }

    "same source, each branch length 2" in new TestEnvironment {
      //  x -[1]-> y     y -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:                 x
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3                                     2 -> 5  -> y: 6 -> z: 2,3,7    |\ 1 -> [5] -> 3*3  = 1 * 9 = 9
      // 1 -> 2, 3, 4  r   2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2                                            -> a: 6 -> b: 2,3,7    |/                                 + = 13
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4                                     3 -> 6  -> y: 4 -> z: 6,7      |\ 1 -> [6] -> 2*2  =  1 * 4 = 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9                                           -> a: 4 -> b: 6,7      |/
      //
      //  x -[1]-> a     a -[2]-> b     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
      // 1 -> 2, 3, 4     2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
      // 6 -> 4           6 -> 2,3,7                                                SUM=9

      // JOIN INDIVIUDAL RPQ RESULTS BY JOINING VALUE ON NODE 'X'
      //  From 1st rpq values for X = 5, 6
      //  Joining value 5: In: 1 Out final: 3
      //  Joining value 6: In: 1 Out final: 2
      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
      val rpq3 = RPQ(QueryNode("x"), QueryNode("a"), 1)
      val rpq4 = RPQ(QueryNode("a"), QueryNode("b"), 2)
      val rpqs = List(rpq1, rpq2, rpq3, rpq4)
      val result = estimator.evaluateCardinality(CRPQ(rpqs))

      result must beEqualTo(29)
    }

//    "testing by part for : length 4: process RPQs length 3 that start on same source and one incoming to that source node" in new IndexEnv {
//      //  x -[1]-> y     y -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:                 x
//      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3                                     2 -> 5  -> y: 6 -> z: 2,3,7    |\ 1 -> [5] -> 3*3  = 1 * 9 = 9
//      // 1 -> 2, 3, 4  r   2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2                                            -> a: 6 -> b: 2,3,7    |/                                 + = 13
//      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4                                     3 -> 6  -> y: 4 -> z: 6,7      |\ 1 -> [6] -> 2*2  =  1 * 4 = 4
//      // 6 -> 4           6 -> 2,3,7                                                SUM=9                                           -> a: 4 -> b: 6,7      |/
//      //
//      //  x -[1]-> a     a -[2]-> b     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
//      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
//      // 1 -> 2, 3, 4     2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
//      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
//      // 6 -> 4           6 -> 2,3,7                                                SUM=9
//
//      //  t -(3)-> x    ==> Joining values: 5,6    Pre 5 possibilities: 1 (value=2)
//      //  2 ->  5,7                                Pre 6 possibilities: 1 (value=3)
//      //  3 ->  4,6
//      //  4 ->  3
//
//      // JOIN INDIVIUDAL RPQ RESULTS BY JOINING VALUE ON NODE 'X'
//      //  From 1st rpq values for X = 5, 6
//      //  Joining value 5: In: 1 Out final: 3
//      //  Joining value 6: In: 1 Out final: 2
//            val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
//            val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
//      val rpq3 = RPQ(QueryNode("x"), QueryNode("a"), 1)
//      val rpq4 = RPQ(QueryNode("a"), QueryNode("b"), 2)
//      val rpq5 = RPQ(QueryNode("t"), QueryNode("x"), 3)
//      val rpqs = List(rpq1, rpq2, rpq3, rpq4, rpq5)
//      val result = estimator.estimate(rpqs)
//
//      result must beEqualTo(13)
//    }

//    "length 4: process RPQs length 3 that start on same source and one incoming to that source node" in new IndexEnv {
//      //  x -[1]-> y     y -[2]-> z     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:                 x
//      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3                                     2 -> 5  -> y: 6 -> z: 2,3,7    |\ 1 -> [5] -> 3*3  = 1 * 9 = 9
//      // 1 -> 2, 3, 4  r   2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2                                            -> a: 6 -> b: 2,3,7    |/                                 + = 13
//      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4                                     3 -> 6  -> y: 4 -> z: 6,7      |\ 1 -> [6] -> 2*2  =  1 * 4 = 4
//      // 6 -> 4           6 -> 2,3,7                                                SUM=9                                           -> a: 4 -> b: 6,7      |/
//      //
//      //  x -[1]-> a     a -[2]-> b     RESULT OF CONCATENATION is        TOTAL POSSIBILITIES BY JOININIG VALUE:
//      // 5 -> 6           4 -> 6, 7             =>         5  -> 6 ->  2,3,7     1*3= 3
//      // 1 -> 2, 3, 4     2 -> 3,6                         1  -> 2 ->  3,6       1*2= 2
//      // 9 -> 10          5  -> 7                        1,6  -> 4 ->  6,7       2*2= 4
//      // 6 -> 4           6 -> 2,3,7                                                SUM=9
//
//      //  t -(3)-> x    ==> Joining values: 5,6    Pre 5 possibilities: 1 (value=2)
//      //  2 ->  5,7                                Pre 6 possibilities: 1 (value=3)
//      //  3 ->  4,6
//      //  4 ->  3
//
//      // JOIN INDIVIUDAL RPQ RESULTS BY JOINING VALUE ON NODE 'X'
//      //  From 1st rpq values for X = 5, 6
//      //  Joining value 5: In: 1 Out final: 3
//      //  Joining value 6: In: 1 Out final: 2
//      val rpq1 = RPQ(QueryNode("x"), QueryNode("y"), 1)
//      val rpq2 = RPQ(QueryNode("y"), QueryNode("z"), 2)
//      val rpq3 = RPQ(QueryNode("x"), QueryNode("a"), 1)
//      val rpq4 = RPQ(QueryNode("a"), QueryNode("b"), 2)
//      val rpq5 = RPQ(QueryNode("t"), QueryNode("x"), 3)
//      val rpqs = List(rpq1, rpq2, rpq3, rpq4, rpq5)
//      val result = estimator.estimate(rpqs)
//
//      result must beEqualTo(13)
//    }

  }

//  trait IndexEnv extends Scope with KPathIndexTestUtils {
//
//    val estimator = new CRPQCardinalityEvaluator(kPathIndex, new QueryResult)
//    //empty QueryResult before each test
//  }
}

trait TestEnvironment extends Scope {
  val kPathIndex = new KPathIndexInMemory()
  val labels = List("6", "8", "1")
  val id1 = PathIdStore.getPathIdByEdges(List(Edge("6")))
  val id2 = PathIdStore.getPathIdByEdges(List(Edge("8")))
  val id3 = PathIdStore.getPathIdByEdges(List(Edge("1")))
  kPathIndex.insert(Path(id1, List(Node(1), Node(2))))
  kPathIndex.insert(Path(id1, List(Node(1), Node(3))))
  kPathIndex.insert(Path(id1, List(Node(1), Node(4))))
  kPathIndex.insert(Path(id1, List(Node(5), Node(6))))
  kPathIndex.insert(Path(id1, List(Node(6), Node(4))))
  kPathIndex.insert(Path(id1, List(Node(9), Node(10))))

  kPathIndex.insert(Path(id2, List(Node(2), Node(3))))
  kPathIndex.insert(Path(id2, List(Node(2), Node(6))))
  kPathIndex.insert(Path(id2, List(Node(4), Node(6))))
  kPathIndex.insert(Path(id2, List(Node(4), Node(7))))
  kPathIndex.insert(Path(id2, List(Node(5), Node(7))))
  kPathIndex.insert(Path(id2, List(Node(6), Node(7))))
  kPathIndex.insert(Path(id2, List(Node(6), Node(2))))
  kPathIndex.insert(Path(id2, List(Node(6), Node(3))))

  kPathIndex.insert(Path(id3, List(Node(2), Node(5))))
  kPathIndex.insert(Path(id3, List(Node(2), Node(7))))
  kPathIndex.insert(Path(id3, List(Node(3), Node(4))))
  kPathIndex.insert(Path(id3, List(Node(3), Node(6))))
  kPathIndex.insert(Path(id3, List(Node(4), Node(3))))

  val estimator = new CRPQCardinalityEvaluator(kPathIndex, new QueryResult)
}
