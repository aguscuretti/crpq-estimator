package bbk.curetti

import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

abstract class BaseTest(runInParallel: Boolean = false) extends Specification with Mockito {

  if (!runInParallel) {
    sequential
    println("Tests will run in sequentially!")
  } else {
    println("Tests will run in parallel!")
  }
}
