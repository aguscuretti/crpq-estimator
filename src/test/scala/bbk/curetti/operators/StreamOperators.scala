package bbk.curetti.operators

import bbk.curetti.BaseTest
import bbk.curetti.model.graph.{Edge, Node, Path}
import bbk.curetti.model.stores.PathIdStore
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner


@RunWith(classOf[JUnitRunner])
class StreamOperatorsTest extends BaseTest{
  "merging paths" should {

    "indexPathsByLastNode" in {
      val p1 = Path(0, List(Node(3), Node(4)))
      val p2 = Path(0, List(Node(1), Node(4)))
      val p3 = Path(1, List(Node(1), Node(5)))
      val stream: Stream[Path] = Stream(p1, p2, p3)

      val result = StreamOperators.indexPathsByLastNode(stream)

      result should contain (4 -> Stream(p1, p2))
      result should contain (5 -> Stream(p3))

    }

    "merge paths streams" in {
      val path1 = Path(1, List(Node(1), Node(3)))
      val path2 = Path(2, List(Node(2), Node(4)))

      val path3 = Path(1, List(Node(3), Node(4)))
      val path4 = Path(2, List(Node(4), Node(5)))
      val stream1 = Stream(path1, path2)
      val stream2 = Stream(path3, path4)

      // Add paths for `edge1 - edge1` and `edge2 - edge2` to the PathIdStore
      val id3 = PathIdStore.getPathIdByEdges(List(Edge("edge1"), Edge("edge1")))
      val id4 = PathIdStore.getPathIdByEdges(List(Edge("edge2"), Edge("edge2")))

      val result: Stream[Path] = StreamOperators.hashjoin(stream1, stream2)

      val p1 = Path(3, List(Node(1), Node(3), Node(4)))
      val p2 = Path(4, List(Node(2), Node(4), Node(5)))
      val expected: Stream[Path] = Stream.cons(p1, Stream.cons(p2, Stream.empty))

      result must containTheSameElementsAs(expected)
    }
  }

}