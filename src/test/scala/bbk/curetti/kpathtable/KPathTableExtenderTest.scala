package bbk.curetti.kpathtable

import bbk.curetti.BaseTest
import bbk.curetti.kpathindex.KPathIndexInMemory
import bbk.curetti.model.graph.{Node, Path}
import bbk.curetti.model.query.{CRPQ, QueryNode, RPQ}
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner


@RunWith(classOf[JUnitRunner])
class KPathTableExtenderTest extends BaseTest{

  "KPathTableCreator" should {

    "add to the k-path table an entry for the pathId of each RPQ of the CRPQ" in {
      val kPathIndex = spy(new KPathIndexInMemory)
      // ids don't need to be fetched from the PathIdStore for the means of this test
      val paths = List(
        Path(1, List(Node(1), Node(2))), Path(1, List(Node(11), Node(22))),
        Path(2, List(Node(2), Node(3), Node(4))), Path(2, List(Node(22), Node(33), Node(44))), Path(2, List(Node(7), Node(8), Node(9))),
        Path(3, List(Node(2), Node(5))))
      paths.foreach(path => kPathIndex.insert(path))

      val rpq1 = RPQ(QueryNode("a"), QueryNode("b"), 1)
      val rpq2 = RPQ(QueryNode("b"), QueryNode("c"), 2)
      val rpq3 = RPQ(QueryNode("a"), QueryNode("c"), 3)
      val crpq = CRPQ(List(rpq1, rpq2, rpq3))

      val kPathTable = spy(new KPathTableInMemory)
      KPathTableExtender.run(kPathIndex, kPathTable, 1, Some(crpq))
      there was one(kPathTable).insert(1, PathIdStats(sources = Set(1, 11), targets = Set(2, 22)))
      there was one(kPathTable).insert(2, PathIdStats(sources = Set(2, 22, 7), targets = Set(4, 44, 9)))
      there was one(kPathTable).insert(3, PathIdStats(sources = Set(2), targets = Set(5)))
    }

  }
}

