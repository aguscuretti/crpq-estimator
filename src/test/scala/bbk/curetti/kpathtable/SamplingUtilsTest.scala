package bbk.curetti.kpathtable

import bbk.curetti.BaseTest
import bbk.curetti.sampling.SamplingUtils
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class SamplingUtilsTest extends BaseTest{

  "SamplingUtils" should {

    "take sample" in {
      val input = Seq.fill(100)(Random.nextInt())
      val (sample1, count1) = SamplingUtils.reservoirSampleAndCount(input.iterator, 10)
      sample1.size must beEqualTo(10)

    }
  }
}

