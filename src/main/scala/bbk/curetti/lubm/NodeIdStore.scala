package bbk.curetti.lubm

import bbk.curetti.model.stores.LongCounter

import scala.collection.immutable.ListMap

class NodeIdStore(mapping: Map[String, Long])

object NodeIdStore {
  private var idGenerator = new LongCounter()
  private var nodeIdStore = Map[Long, String]()
  private var nodeNameStore = Map[String, Long]()

  private def generateNodeId(name: String): Long = {
    val id = idGenerator.incrementAndGet()
    nodeIdStore += (id -> name)
    nodeNameStore += (name -> id)
    id
  }

  def getNodeId(name: String): Long = {
    if(nodeNameStore.contains(name)){
      nodeNameStore.getOrElse(name, -1)
    } else {
      generateNodeId(name)
    }
  }

  def getSize : Long = {
    nodeIdStore.keys.size
  }
}

class LabelIdStore(mapping: Map[String, Long])

object LabelIdStore {
  private var idGenerator = new LongCounter()
  private var labelIdStore = Map[Long, String]()
  private var labelNameStore = Map[String, Long]()

  private def generateLabelId(name: String): Long = {
    val id = idGenerator.incrementAndGet()
    labelIdStore += (id -> name)
    labelNameStore += (name -> id)
    id
  }

  def getLabelId(name: String): Long = {
    if(labelNameStore.contains(name)){
      labelNameStore.getOrElse(name, -1)
    } else {
      generateLabelId(name)
    }
  }

  def printAllLabels() = {
    ListMap(labelIdStore.toSeq.sortBy(_._1):_*).foreach(elem => println(s"${elem._1}: ${elem._2}"))
  }

}

trait IdStore {
  private var idGenerator = new LongCounter()
  private var idStore = Map[Long, String]()
  private var nameStore = Map[String, Long]()

  private def generateId(name: String): Long = {
    val id = idGenerator.incrementAndGet()
    idStore += (id -> name)
    nameStore += (name -> id)
    id
  }

  def getLabelId(name: String): Long = {
    if(nameStore.contains(name)){
      nameStore.getOrElse(name, -1)
    } else {
      generateId(name)
    }
  }

  def printElements: Unit = {
    ListMap(idStore.toSeq.sortBy(_._1):_*).foreach(elem => println(s"${elem._1}: ${elem._2}"))
  }
}

