package bbk.curetti

import java.io.{BufferedWriter, FileWriter}

import bbk.crpq.pathIndex.IndexExtender
import bbk.curetti.conf.GraphConfig
import bbk.curetti.estimator._
import bbk.curetti.evaluator.{CRPQCardinalityEvaluator, QueryResult}
import bbk.curetti.kpathindex.{FileImporter, KPathIndex, KPathIndexInMemory}
import bbk.curetti.kpathtable.{KPathTableExtender, KPathTableInMemory}
import bbk.curetti.model.query.CRPQ
import bbk.curetti.parser.{CRPQIdStore, CRPQParser}
import com.opencsv.CSVWriter

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object SamplingStrategy {
  val bernoulli = "bernoulli"
  val indexAssisted = "index_assisted"
}

object Main {

  private val samplingFlag = true
  private val kPathIndex = new KPathIndexInMemory()
  private val graphConfig = new GraphConfig()
  //val dbName = "advogato" // snapRER  lubm  cfindergoogle egogplus

  def main(args: Array[String]): Unit = {
	  val dbName = args(0).toString
    val numExperimentsPerSamplingFactor = 1
    val inputDataFileName = graphConfig.envOrElseConfig(s"datasets.$dbName.data")
    val graphLabels = graphConfig.envOrElseConfig(s"datasets.$dbName.labels")
    val separator = graphConfig.envOrElseConfig(s"datasets.$dbName.separator")
    val labels: List[String] = graphLabels.split(separator).toList

    val kForDataset = graphConfig.envOrElseConfig(s"datasets.$dbName.k").toInt
    println(s"K index for $dbName: $kForDataset")
    setUpIndex(kPathIndex, inputDataFileName, labels, kForDataset)

    //    val takesCourseTaughtById = getPathIdByEdges(List(Edge("takesCourse"), Edge("teacherOf")))
    //    val paths = kPathIndex.search(PathPrefix(takesCourseTaughtById, List.empty))
    //    println(s"takesCourse-teacherOf size: ${paths.size}")
    val queriesFileName = graphConfig.envOrElseConfig(s"datasets.$dbName.queries")
    val crpqs = parseQueries(queriesFileName, labels)

    //    val samplingFactorsToTest = List(0.2, 0.3, 0.4, 0.5, 0.7, 0.8, 1.0)
    val samplingFactors = List(1.0) //0.2, 0.3, 0.4, 0.5, 0.7, 0.8, 1.0
    val testStrategies = List(SamplingStrategy.bernoulli, SamplingStrategy.indexAssisted)

    val bernoulliResults = mutable.Map[(CRPQ, Double), List[ExperimentResult]]() // Map[ (crpq, SamplingFactor) -> List(non_correlated_results) ]
    val indexAssistedResults = mutable.Map[(CRPQ, Double), List[ExperimentResult]]() // Map[ (crpq, SamplingFactor) -> List(correlated_results scaled up With sampling factor) ]

    crpqs.foreach { crpq =>
      val crpqId = CRPQIdStore.getCRPQId(crpq)
    }
    CRPQIdStore.printAllCrpqs()

    crpqs.foreach { crpq =>
      val crpqId = CRPQIdStore.getCRPQId(crpq)
      println(s"\nProcessing crpq id $crpqId")
      val crpqEvaluator = new CRPQCardinalityEvaluator(kPathIndex, new QueryResult())
      val t0 = System.nanoTime()
      val realCardinality = crpqEvaluator.calculateCardinality(crpq, Map.empty)
      val t1 = System.nanoTime()
      val evaluationTime = (t1 - t0)
      println(s"\nNEW CRPQ: ${CRPQ.prettyPrint(crpq)} Cardinality: $realCardinality")

      if (samplingFlag) {
        if (realCardinality > 0) {
          //prepare k-path table that contains all possible values for each node in CRPQ
          println(s"Making k-path table...")
          val t2 = System.nanoTime()
          val kPathTable = new KPathTableInMemory()
          KPathTableExtender.run(kPathIndex, kPathTable, 1.0, Some(crpq))
          val t3 = System.nanoTime()
          println(s"Time to build k-path table ${convertNsToSecs(t3 - t2)}")

          samplingFactors.foreach { samplingFactor =>
            val sfBernoulliResults = ListBuffer[ExperimentResult]()
            val sfIndexAssistedResults = ListBuffer[ExperimentResult]()

            (1 to numExperimentsPerSamplingFactor).foreach { experimentNumber =>
              val estimatorIndexAssisted = new IndexAssistedSamplingCRPQEstimator(kPathTable, kPathIndex, samplingFactor)
              executeExperiment(dbName, experimentNumber, SamplingStrategy.indexAssisted, crpq, realCardinality, samplingFactor, sfIndexAssistedResults, estimatorIndexAssisted)

              val estimatorBernoulli = new SimpleRandomSamplingCRPQEstimator(kPathTable, kPathIndex, samplingFactor)
              executeExperiment(dbName, experimentNumber, SamplingStrategy.bernoulli, crpq, realCardinality, samplingFactor, sfBernoulliResults, estimatorBernoulli)
            }
            bernoulliResults.update((crpq, samplingFactor), sfBernoulliResults.toList)
            indexAssistedResults.update((crpq, samplingFactor), sfIndexAssistedResults.toList)
          }
          printEstimationResults
        } else {
          print(s"Evaluation result: CRPQ has 0 answers. Continuing with the next...")
        }
      } else {
        println(s"Continue with next crpq because card was 0...\n")
      }
    }

    println(s"---------------------- printing CRPQs map for dataset $dbName:")
    CRPQIdStore.printAllCrpqs()

    def printEstimationResults = {
      var avgResultsBernoulli = Seq[StrategyAverageResultPerSf]()
      var avgResultsIndexAssisted = Seq[StrategyAverageResultPerSf]()
      testStrategies.foreach { strategy =>
        strategy match {
          case SamplingStrategy.bernoulli =>
            avgResultsBernoulli = calculateAverageResults(bernoulliResults)
            printStrategyResults(avgResultsBernoulli, dbName)
            /** per strategy: take avg error measures for each sf to plot (sf -> error rate) */
            val avgErrorsPerSf = avgDataToPlot(avgResultsBernoulli)
            printDataToPlot(avgErrorsPerSf, SamplingStrategy.bernoulli, dbName)

          case SamplingStrategy.indexAssisted =>
            avgResultsIndexAssisted = calculateAverageResults(indexAssistedResults)
            printStrategyResults(avgResultsIndexAssisted, dbName)
            val avgErrorsPerSf = avgDataToPlot(avgResultsIndexAssisted)
            printDataToPlot(avgErrorsPerSf, SamplingStrategy.indexAssisted, dbName)
        }
      }
      /** Print average results all together for each (crpq, sf) */
      val allResults = mergeBySf(avgResultsBernoulli.toList, avgResultsIndexAssisted.toList) //these are the GroupedAverageResultPerSf
      printResults(allResults, dbName)
    }
  }

  def avg(xs: List[Double]) = xs.sum / xs.length

  def avgDataToPlot(results: Seq[StrategyAverageResultPerSf]): List[SamplingFactorAvgError] = {
    val dataToPlot = results.groupBy(_.sf).map { kv =>
      SamplingFactorAvgError(
        kv._1,
        formatToDecimals(avg(kv._2.map(_.errorRate).toList), 4),
        formatToDecimals(avg(kv._2.map(_.qError).toList), 4))
    }
    dataToPlot.toList.sortBy(_.sf)
  }

  private def executeExperiment(dbName: String,
								experimentNumber: Int,
                                strategy: String,
                                crpq: CRPQ,
                                realCardinality: BigInt,
                                samplingFactor: Double,
                                resultsBuffer: ListBuffer[ExperimentResult],
                                estimator: AbstractCRPQCardinalityEstimator): BigInt = {
    println(s"\nExp #$experimentNumber Strategy: $strategy CRPQ: ${CRPQ.prettyPrint(crpq)}")
    val t0 = System.nanoTime()
    val estimation = estimator.estimateCardinality(crpq)
    val t1 = System.nanoTime()
    val time = (t1 - t0)
    val timeFormatted: Double = convertNsToSecs(time)
    val expResultNonCorr = ExperimentResult(dbName, crpq, realCardinality, samplingFactor, strategy, estimation, timeFormatted)
    resultsBuffer += expResultNonCorr
    println(s"Results Exp#$experimentNumber Strategy: $strategy CRPQ: ${CRPQ.prettyPrint(crpq)} (sf:$samplingFactor)-> Real = $realCardinality Est = $estimation diff (E-R): ${estimation - realCardinality}  " +
      s"\nMean error: (E-R)/R: ${(estimation - realCardinality).doubleValue() / realCardinality.toDouble}")
    estimation
  }

  def setUpIndex(kPathIndex: KPathIndex, filename: String, labels: List[String], k: Int) = {
    val t0 = System.nanoTime()
    val lines = FileImporter.buildBaseIndex(kPathIndex, filename, labels)
    val t1 = System.nanoTime()
    println(s"Imported lines: $lines")
    println("Elapsed time to build Base Index: " + convertNsToSecs(t1 - t0) + "secs.")

    val t2 = System.nanoTime()
    IndexExtender.run(kPathIndex, k)
    val t3 = System.nanoTime()
    println(s"Elapsed time to extend k-path index to k=$k: " + convertNsToSecs(t3 - t2) + "secs.")
  }

  private def convertNsToSecs(ns: Long): Double = {
    BigDecimal(ns / 1000000000.0).setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  private def parseQueries(filename: String, labels: List[String]): List[CRPQ] = {
    CRPQParser.parse(filename, labels)
  }

  private def calculateAverageResults(resultsMap: mutable.Map[(CRPQ, Double), List[ExperimentResult]]): List[StrategyAverageResultPerSf] = {
    val strategyName = resultsMap.head._2.head.samplingStrategy

    val avgResults = ListBuffer[StrategyAverageResultPerSf]()

    resultsMap.foreach { case ((crpq, sf), results) =>
      val record = results.head
      val estimations = results.map(_.estimatedCardinality)
      val avgEstimation = mean(estimations)
      val avgEstimationTime = results.map(_.elapsedTimeEstimation).sum / results.size.toDouble

      val estimationDouble: Double = avgEstimation.doubleValue()
      val cardinalityDouble: Double = record.realCardinality.toDouble
      val errorRate = if (avgEstimation == record.realCardinality) 0
      else ((math.abs(estimationDouble - cardinalityDouble)) / math.max(estimationDouble, cardinalityDouble))

      val nPrima = Math.max(cardinalityDouble, 1)
      val ePrima = Math.max(estimationDouble, 1)
      val qError = Math.max((nPrima / ePrima), (ePrima / nPrima))

      if(qError < 50) { //leave out very bad CRPQs
        val sfAvgResult = StrategyAverageResultPerSf(
          strategyName,
          record.dbName,
          record.crpq,
          record.realCardinality,
          sf,
          avgEstimation,
          formatToDecimals(errorRate, 4),
          formatToDecimals(qError, 4),
          avgEstimationTime)

        avgResults += sfAvgResult
      }
    }
    avgResults.groupBy(r => r.crpq).mapValues(_.toList.sortBy(_.sf)).values.flatten.toList
  }

  def formatToDecimals(number: Double, decimals: Int): Double = {
    BigDecimal(number).setScale(decimals, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  def printResults(rows: Seq[GroupedAverageResultPerSf], dbName: String) = {
    val avgResultsTogetherFile = s"results/$dbName/${dbName}_avg_results_all_strategies.csv"
    val outWriter = new BufferedWriter(new FileWriter(avgResultsTogetherFile))
    val csvWriter = new CSVWriter(outWriter)
    val resultSchema = Array("Database", "CRPQ", "Real Cardinality", "Sampling Factor",
      "Independent Bernoulli Estimated Card", "Error Rate", "q-error",
      "Index-assisted Estimated Card", "Error Rate", "q-error")
    csvWriter.writeNext(resultSchema)

    rows.foreach { row =>
      csvWriter.writeNext(createGroupedAverageResultPerSf(row))
    }
    csvWriter.close()
  }

  def printDataToPlot(dataToPlotNonCorr: List[SamplingFactorAvgError], strategy: String, dbName: String) = {
    val avgErrorRateFile = s"results/$dbName/${dbName}_${strategy}_plot_avg_ERROR_rate.txt"
    val outWriter = new BufferedWriter(new FileWriter(avgErrorRateFile))
    val csvWriterErrorRate = new CSVWriter(outWriter)
    val resultSchema = Array("Sampling Factor", "Average Error Rate")
    csvWriterErrorRate.writeNext(resultSchema)

    val avgQErrorFile = s"results/$dbName/${dbName}_${strategy}_plot_avg_Q_error.txt"
    val outWriterQError = new BufferedWriter(new FileWriter(avgQErrorFile))
    val csvWriterQError = new CSVWriter(outWriterQError)
    val resultSchemaQError = Array("Sampling Factor", "Average q-error")
    csvWriterQError.writeNext(resultSchemaQError)

    dataToPlotNonCorr.foreach { row =>
      csvWriterErrorRate.writeNext(createDataToPlotAvgErrorRate(row))
      csvWriterQError.writeNext(createDataToPlotAvgQError(row))
    }

    csvWriterErrorRate.close()
    csvWriterQError.close()
  }

  def printStrategyResults(results: Seq[StrategyAverageResultPerSf], dbName: String) = {
    val strategy = results.head.strategyName
    val fileName = s"results/$dbName/${dbName}_${strategy}_avg_results.csv"
    val outWriter = new BufferedWriter(new FileWriter(fileName))
    val csvWriter = new CSVWriter(outWriter)
    val resultSchema = Array("Strategy", "Database", "CRPQ", "Real Cardinality", "Sampling Factor",
      "Estimated Cardinality", "Mean Error Rate", "q-error", "Estimation time (secs)")
    csvWriter.writeNext(resultSchema)

    results.foreach { result =>
      csvWriter.writeNext(createStrategyResultPerSfRow(result))
    }
    csvWriter.close()
  }

  def mergeBySf(avgResultsBernoulli: List[StrategyAverageResultPerSf],
                avgResultsIndexAssisted: List[StrategyAverageResultPerSf]): List[Main.GroupedAverageResultPerSf] = {

    val allZipped = (avgResultsBernoulli, avgResultsIndexAssisted).zipped.toList

    allZipped.map { tuple =>
      val dbName = tuple._1.dbName
      val crpq = tuple._1.crpq
      val realCard = tuple._1.realCardinality
      val sf = tuple._1.sf

      val avgEstimationBernoulli = tuple._1.avgEstimation
      val avgErrorRateBernoulli = tuple._1.errorRate
      val avgQErrorBernoulli = tuple._1.qError

      val avgEstimationIndexAssisted = tuple._2.avgEstimation
      val avgErrorRateIndexAssisted = tuple._2.errorRate
      val avgQErrorIndexAssisted = tuple._2.qError


      GroupedAverageResultPerSf(dbName, crpq, realCard, sf,
          avgEstimationBernoulli, avgErrorRateBernoulli, avgQErrorBernoulli,
          avgEstimationIndexAssisted, avgErrorRateIndexAssisted, avgQErrorIndexAssisted)


    }
  }

  case class SamplingFactorAvgError(sf: Double, avgError: Double, avgQError: Double)

  def createDataToPlotAvgErrorRate(row: SamplingFactorAvgError): Array[String] = {
    Array(s"(${row.sf.toString} , ${row.avgError.toString})")
  }

  def createDataToPlotAvgQError(row: SamplingFactorAvgError): Array[String] = {
    Array(s"(${row.sf.toString} , ${row.avgQError.toString})")
  }

  case class ExperimentResult(dbName: String,
                              crpq: CRPQ,
                              realCardinality: BigInt,
                              samplingFactor: Double,
                              samplingStrategy: String,
                              estimatedCardinality: BigInt,
                              elapsedTimeEstimation: Double)

  def createResultRow(result: ExperimentResult): Array[String] = {
    Array(result.dbName,
      CRPQ.prettyPrint(result.crpq),
      result.samplingFactor.toString,
      result.realCardinality.toString,
      result.estimatedCardinality.toString,
      result.elapsedTimeEstimation.toString)
  }

  case class StrategyAverageResultPerSf(strategyName: String,
                                        dbName: String,
                                        crpq: CRPQ,
                                        realCardinality: BigInt,
                                        sf: Double,
                                        avgEstimation: BigInt,
                                        errorRate: Double,
                                        qError: Double,
                                        avgEstimationTime: Double)

  def createStrategyResultPerSfRow(result: StrategyAverageResultPerSf): Array[String] = {
    Array(
      result.strategyName,
      result.dbName,
      CRPQ.prettyPrint(result.crpq),
      result.realCardinality.toString,
      result.sf.toString,
      result.avgEstimation.toString,
      result.errorRate.toString,
      result.qError.toString,
      result.avgEstimationTime.toString)
  }

  case class GroupedAverageResultPerSf(dbName: String,
                                       crpq: CRPQ,
                                       realCardinality: BigInt,
                                       sf: Double,
                                       avgEstimationBernoulli: BigInt,
                                       avgErrorRateBernoulli: Double,
                                       avgQErrorBernoulli: Double,
                                       avgEstimationIndexAssisted: BigInt,
                                       avgErrorRateIndexAssisted: Double,
                                       avgQErrorIndexAssisted: Double
                                      )

  def createGroupedAverageResultPerSf(result: GroupedAverageResultPerSf): Array[String] = {
    Array(
      result.dbName,
      CRPQ.prettyPrint(result.crpq),
      result.realCardinality.toString,
      result.sf.toString,
      result.avgEstimationBernoulli.toString,
      result.avgErrorRateBernoulli.toString,
      result.avgQErrorBernoulli.toString,
      result.avgEstimationIndexAssisted.toString,
      result.avgErrorRateIndexAssisted.toString,
      result.avgQErrorIndexAssisted.toString
    )
  }


  def mean(xs: List[BigInt]): BigInt = xs match {
    case Nil => 0
    case ys => ys.reduceLeft(_ + _) / BigInt(ys.size)
  }

  def stddev(xs: List[BigInt], avg: BigInt): BigInt = xs match {
    case Nil => 0
    case ys => sqrt((BigInt(0) /: ys) {
      (a, e) => a + ((e - avg).pow(2))
    } / (xs.size - 1))
  }

  private def sqrt(number: BigInt): BigInt = {
    def next(n: BigInt, i: BigInt): BigInt = (n + i / n) >> 1

    val one = BigInt(1)
    val n = one
    val n1 = next(n, number)

    def sqrtHelper(n: BigInt, n1: BigInt): BigInt = if ((n1 - n).abs <= one) List(n1, n).max else sqrtHelper(n1, next(n1, number))

    sqrtHelper(n, n1)
  }
}

