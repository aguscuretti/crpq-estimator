package bbk.curetti.model.stores


class LongCounter {
  private var count: Long = _

  private def increment(): Unit = count += 1

  def incrementAndGet(): Long = {
    increment()
    count
  }
}