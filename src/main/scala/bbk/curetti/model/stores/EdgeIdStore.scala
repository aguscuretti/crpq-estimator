package bbk.curetti.model.stores

import bbk.curetti.model.graph.Edge


class EdgeIdStore(mapping: Map[String, Long])

object EdgeIdStore {
  private var edgeIdStore = Map[Long, String]()
  private var edgeLabelStore = Map[String, Long]()
  private var idGenerator = new LongCounter()

  def getEdgeId(edge: Edge): Long = {
    if(edgeLabelStore.contains(edge.label)) {
      edgeLabelStore.getOrElse(edge.label, -1)
    } else
      generateEdgeId(edge.label)
  }

  def generateEdgeId(label: String): Long = {
    val id = idGenerator.incrementAndGet()
    edgeLabelStore += (label -> id)
    edgeIdStore += (id -> label)
    id
  }

  def getEdge(edgeId: Long): Edge = {
    if(edgeIdStore.contains(edgeId)) {
      Edge(edgeIdStore.getOrElse(edgeId, ""))
    } else
        throw new IllegalArgumentException(s"edgeId $edgeId is not contained in EdgeIdStore")
  }

}

