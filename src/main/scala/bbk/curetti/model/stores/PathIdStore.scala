package bbk.curetti.model.stores

import bbk.curetti.model.graph.{Edge, Path}
import grizzled.slf4j.Logging

import scala.collection.mutable.ListBuffer

class PathIdStore(mapping: Map[String, Long])

object PathIdStore extends Logging {
    private var pathIdStore = Map[Long, String]()
    private var pathEdgeStore = Map[String, Long]()
    private var kPathIdStore = Map[Int, ListBuffer[Long]]()
    private val edgeSeparator = ","
    private var id: Long = 0

    def getPathIdByEdgeLabel(label: String): Long = {
        if (pathEdgeStore.contains(label))
            return pathEdgeStore.getOrElse(label, -1)
        else
            throw new IllegalArgumentException(s"PathEdgeStore does not contain label $label")
    }

    def serializeEdgesIds(edges: List[Edge]): String = {
        edges.map(edge => EdgeIdStore.getEdgeId(edge).toString).mkString(edgeSeparator)
    }

    def deserializeEdgesIds(serializedEdges: String): List[Edge] = {
        serializedEdges.split(edgeSeparator).map(id => EdgeIdStore.getEdge(id.toLong)).toList
    }

    /**
      * Get all paths of length k
      */
    def getPathsIdLengthK(k: Int): List[Long] = {
        return kPathIdStore.getOrElse(k, List.empty[Long]).toList
    }

    def generatePathId(edges: List[Edge], serialized: String): Long = {
        debug(s"Generate id for path with edges: ${edges} serialized: $serialized")
        id = id + 1
        pathEdgeStore += (serialized -> id)
        pathIdStore += (id -> serialized)

        val kPathIds = kPathIdStore.getOrElse(edges.size, ListBuffer.empty)
        if(kPathIds.isEmpty){
            kPathIdStore += (edges.size -> ListBuffer(id))
            debug(s"New path for k size: ${edges.size} -> ${edges}")
        } else {
            kPathIdStore.updated(edges.size, kPathIds += id)
            debug(s"Added path for k size: ${edges.size} -> ${edges}")
        }
        id
    }

    def getPathIdByEdges(edges: List[Edge]): Long = {
        val serializedPath = serializeEdgesIds(edges)
        return pathEdgeStore.getOrElse(serializedPath, generatePathId(edges, serializedPath))
    }

    def merge(path1: Path, path2: Path): Path = {
        val newPathEdges = getEdges(path1.pathId) ::: getEdges(path2.pathId)
        val newPathNodes = path1.nodes ::: path2.nodes.drop(1)
        return Path(getPathIdByEdges(newPathEdges), newPathNodes)
    }

    def getEdges(pathId: Long): List[Edge] = {
        if(pathIdStore.contains(pathId))
            return deserializeEdgesIds(pathIdStore.get(pathId).get)
        else
            throw new IllegalArgumentException(s"PathIdStore does not contain pathId: $pathId")
    }

    def prettyPrintPath(pathId: Long): String = {
        val edges = getEdges(pathId)
        edges.map(_.label).mkString(" ")
    }

    def printCdotPath(pathId: Long): String = {
        val edges = getEdges(pathId)
        edges.map(_.label).mkString(" ").replaceAll(" ", """ \\cdot """)
    }
}
