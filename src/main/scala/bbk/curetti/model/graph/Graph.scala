package bbk.curetti.model.graph

case class Node(id: Long)

case class Edge(label: String)

/**
  * A path as it is indexed
  *
  * @param pathId   the long id representing the k-path 0 -> .6, 9 -> .6,.8,.8,1
  * @param nodes    the list of nodes in the path
  */
case class Path(pathId: Long, nodes: List[Node])

/**
  * Used for searching in the pathIndex
  *
  * @param pathId   the id of a specific path. Using the PathIdStore we can retrieve the edges
  * @param nodes    ordered list of nodes along the path to restrict the query
  */
case class PathPrefix(pathId: Long, nodes: List[Node] = List())