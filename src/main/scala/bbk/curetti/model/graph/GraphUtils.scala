package bbk.curetti.model.graph

class GraphUtils {

  def allPossiblePathsToLengthK(labels: List[Int], k: Int): List[List[Int]] = {
    var paths = List[List[Int]]()
    for(pathLength <- 1 to k){
      val tmp = permutations(labels, pathLength).toList
      paths = paths ++ tmp
    }
    paths
  }

  def permutations[X](iter: List[X], len: Int) = {
    def acc(items: List[X], count: Int): Stream[List[X]] = {
      if (count == 0) Stream(items) else iter.toStream.flatMap( x => acc(x :: items, count - 1))
    }
    acc(List.empty[X], len)
  }

}
