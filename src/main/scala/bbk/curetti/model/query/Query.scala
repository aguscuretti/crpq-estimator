package bbk.curetti.model.query

import bbk.curetti.model.stores.PathIdStore

import scala.collection.mutable.ListBuffer


case class QueryNode(id: String, values: List[Long] = List.empty)

case class RPQ(source: QueryNode, target: QueryNode, pathId: Long)

case class CRPQ(rpqs: List[RPQ])


object RPQ {
  def prettyPrint(rpq: RPQ): String = {
    s"${rpq.source.id} -${PathIdStore.prettyPrintPath(rpq.pathId)}-> ${rpq.target.id}"
  }
}
object CRPQ {
  def prettyPrint(crpq: CRPQ): String = {
    var all = List[String]()
    crpq.rpqs.foreach{rpq =>
      all = all :+ RPQ.prettyPrint(rpq)
    }
    all.mkString(",")
  }

  // \begin{tabular}[c]{@{}c@{}}x $\xrightarrow{\text{.6}}$ y\\ y $\xrightarrow{\text{.6}}$ z\end{tabular}
  def latexPrint(crpq: CRPQ): String = {
    val all = ListBuffer[String]()

    val beginTabular = """\begin{tabular}[c]{@{}c@{}}"""
    val endTabular = """\end{tabular}"""
    val dollarSign = """$"""
    val preArrow = """\xrightarrow{\text{"""
    val postArrow = """}}"""
    val newLine = """\\"""

    all.append(beginTabular)
    val len = crpq.rpqs.size

    for (i <- 0 until len) {
      val rpq = crpq.rpqs(i)
      all.append(s"${rpq.source.id} $dollarSign$preArrow ${PathIdStore.prettyPrintPath(rpq.pathId)} $postArrow$dollarSign ${rpq.target.id}")
      if(i < len-1){
        all.append(newLine)
      }
    }

    all.append(endTabular)

    all.mkString(" ")
  }

  // \begin{tabular}[c]{@{}c@{}}x $\xrightarrow{\text{.6}}$ y\\ y $\xrightarrow{\text{.6}}$ z\end{tabular}
  def latexPrintFormalRpq(crpq: CRPQ): String = {
    val all = ListBuffer[String]()

    val beginTabular = """\begin{tabular}[c]{@{}c@{}} \{"""
    val endTabular = """\} \end{tabular}"""
    val dollarSign = """$"""
    val newLine = """, """
    val cdotPath = """$"""

    all.append(beginTabular)
    val len = crpq.rpqs.size

    for (i <- 0 until len) {
      val rpq = crpq.rpqs(i)
      all.append(s"$cdotPath(${rpq.source.id}, ${PathIdStore.printCdotPath(rpq.pathId)}, ${rpq.target.id})$cdotPath")
      if(i < len-1){
        all.append(newLine)
      }
    }

    all.append(endTabular)

    all.mkString(" ")
  }
}
