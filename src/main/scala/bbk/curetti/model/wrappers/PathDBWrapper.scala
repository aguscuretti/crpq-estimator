package bbk.curetti.model.wrappers

import java.util

import bbk.curetti.model.graph.{Node, Path, PathPrefix}
import com.pathdb.pathIndex.models.{Node => NodeDb, Path => PathDb, PathPrefix => PathPrefixDb}

import scala.collection.JavaConverters._

/**
  * Wrapper class for com.pathdb
  */

object PathDBWrapper {

  def toPath(path: Path): PathDb = {
    return com.pathdb.pathIndex.models.ImmutablePath.of(path.pathId, toNodeList(path.nodes).asJava)
  }

  def fromPath(path: PathDb): Path = {
    return Path(path.getPathId, fromNodesList(path.getNodes))
  }

  def toNode(node: Node): NodeDb = {
    return new NodeDb(node.id)
  }

  def fromNode(node: NodeDb): Node = {
    return new Node(node.getId)
  }

  def toNodeList(nodes: List[Node]): List[NodeDb] = {
    nodes.map(node => toNode(node))
  }

  def fromNodesList(nodes: util.List[NodeDb]): List[Node] = {
    nodes.asScala.toStream.map(node => fromNode(node)).toList
  }

  def toPathPrefix(prefix: PathPrefix): PathPrefixDb = {
    return com.pathdb.pathIndex.models.ImmutablePathPrefix.of(prefix.pathId, toNodeList(prefix.nodes).asJava)
  }

}
