package bbk.curetti.parser

import bbk.curetti.kpathindex.FileImporter.getClass
import bbk.curetti.model.graph.Edge
import bbk.curetti.model.query.{CRPQ, QueryNode, RPQ}
import bbk.curetti.model.stores.PathIdStore

import scala.collection.mutable.ListBuffer
import scala.io.Source

object CRPQParser {

  def parse(filename: String, labels: List[String]): List[CRPQ] = {
    var lines = 0
    var crpqs = ListBuffer[CRPQ]()
    val fileStream = getClass.getResourceAsStream(filename)
    val bufferedSource = Source.fromInputStream(fileStream)
    for (line <- bufferedSource.getLines) {
      var crpq = new ListBuffer[RPQ]()
      val rpqs = line.split(",").map(_.trim)
      for(rpq <- rpqs) {
        crpq += parseRPQ(rpq, labels)
      }
      crpqs += CRPQ(crpq.toList)
    }
    bufferedSource.close
    return crpqs.toList
  }

  private def parseRPQ(rpq: String, labels: List[String]): RPQ = {
    val cols = rpq.split(" ").map(_.trim)
    val source = cols(0)
    val target = cols(2)
    val edges = cols(1).split('>').map(label => Edge(label)).toList
    val pathId = PathIdStore.getPathIdByEdges(edges)
    RPQ(QueryNode(source), QueryNode(target), pathId)
  }
}
