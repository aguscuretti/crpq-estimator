package bbk.curetti.parser

import bbk.curetti.model.query.CRPQ
import bbk.curetti.model.stores.LongCounter

import scala.collection.immutable.ListMap


class CRPQIdStore(mapping: Map[CRPQ, Long])

object CRPQIdStore {
  private var idGenerator = new LongCounter()
  private var crpqIdStore = Map[Long, CRPQ]()
  private var crpqNameStore = Map[CRPQ, Long]()

  def generateCRPQId(crpq: CRPQ): Long = {
    val id = idGenerator.incrementAndGet()
    crpqIdStore += (id -> crpq)
    crpqNameStore += (crpq -> id)
    id
  }

  def getCRPQId(crpq: CRPQ): Long = {
    if(crpqNameStore.contains(crpq)){
      crpqNameStore.getOrElse(crpq, -1)
    } else {
      generateCRPQId(crpq)
    }
  }

  def printAllCrpqs() = {
    println(s"\n\nset of crpqs for dataset:")
    ListMap(crpqIdStore.toSeq.sortBy(_._1):_*).foreach{ elem =>
      println(s"""${elem._1}  &  ${CRPQ.latexPrintFormalRpq(elem._2)} """)
    }
  }

}
