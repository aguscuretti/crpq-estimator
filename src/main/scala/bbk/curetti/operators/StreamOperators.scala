package bbk.curetti.operators

import bbk.curetti.model.graph.Path
import bbk.curetti.model.stores.PathIdStore

import scala.collection.mutable

object StreamOperators {

  def hashjoin(streamLengthK: Stream[Path], streamLength1: Stream[Path]): Stream[Path] = {

    val hashMapK: mutable.Map[Long, Stream[Path]] = indexPathsByLastNode(streamLengthK)

    streamLength1.iterator.flatMap { path =>
        val pathsToJoin = hashMapK.getOrElse(path.nodes.head.id, Stream.empty)
        pathsToJoin.map{pathK =>
          PathIdStore.merge(pathK, path)
        }
      }.toStream
  }

  def indexPathsByLastNode(streamK: Stream[Path]): mutable.Map[Long, Stream[Path]] = {
    var indexedPaths = mutable.Map[Long, Stream[Path]]()
    streamK.iterator.foreach { path =>
      val id = path.nodes.last.id

      val paths = indexedPaths.getOrElse(id, Stream.empty[Path])
      if(indexedPaths.contains(id))
        indexedPaths.update(id, paths #::: Stream(path))
      else
        indexedPaths += (id -> Stream(path))
    }
    indexedPaths
  }
}
