package bbk.curetti.kpathindex

import bbk.curetti.model.graph.{Path, PathPrefix}

trait KPathIndex {
  var k: Int
  def insert(path: Path): Unit
  def search(pathPrefix: PathPrefix): Stream[Path]
}
