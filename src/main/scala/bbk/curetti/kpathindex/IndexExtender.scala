package bbk.crpq.pathIndex

import bbk.curetti.kpathindex.KPathIndex
import bbk.curetti.model.graph.{Path, PathPrefix}
import bbk.curetti.model.stores.PathIdStore
import bbk.curetti.operators.StreamOperators
import grizzled.slf4j.Logging

object IndexExtender extends Logging {

  def run(kPathIndex: KPathIndex, kTarget: Int): Long = {
    if (kPathIndex.k >= kTarget)
      return 0
    info(s"Current k: ${kPathIndex.k}, target is $kTarget")

    val pathsLengthK: Stream[Path] = PathIdStore.getPathsIdLengthK(kPathIndex.k)
      .toStream
      .flatMap(path => kPathIndex.search(PathPrefix(path)))

    val pathsLength1: Stream[Path] = PathIdStore.getPathsIdLengthK(1)
      .toStream
      .flatMap(p => kPathIndex.search(PathPrefix(p)))

    // concatenate current path of length K with paths length 1
    val extendedPaths = StreamOperators.hashjoin(pathsLengthK, pathsLength1)

    var count = 0
    extendedPaths.foreach{ path =>
//      info(s"Adding extended path: ${path.pathId} -> ${path.nodes}")
      kPathIndex.insert(path)
      count+=1
    }
    info(s"Finished inserting $count paths of length ${kPathIndex.k}")
    kPathIndex.k += 1
    return run(kPathIndex, kTarget)
  }
}
