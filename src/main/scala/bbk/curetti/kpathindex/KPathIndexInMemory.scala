package bbk.curetti.kpathindex

import bbk.curetti.model.graph.{Path, PathPrefix}
import com.pathdb.pathIndex.inMemoryTree.InMemoryIndexFactory
import bbk.curetti.model.wrappers.PathDBWrapper
import scala.collection.JavaConverters._

class KPathIndexInMemory extends KPathIndex {
  private val pathIndex = new InMemoryIndexFactory().getIndex
  var statisticsStore = pathIndex.getStatisticsStore

  def insert(path: Path): Unit = {
    pathIndex.insert(PathDBWrapper.toPath(path))
  }

  def search(pathPrefix: PathPrefix): Stream[Path] = {
    pathIndex
      .getPaths(PathDBWrapper.toPathPrefix(pathPrefix))
      .asScala
      .map(path => PathDBWrapper.fromPath(path))
      .iterator.toStream
  }

  override var k: Int = _
}
