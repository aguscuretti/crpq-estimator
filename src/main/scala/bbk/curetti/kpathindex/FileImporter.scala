package bbk.curetti.kpathindex

import bbk.curetti.model.graph.{Edge, Node, Path}
import bbk.curetti.model.stores.PathIdStore
import grizzled.slf4j.Logging

import scala.io.Source

object FileImporter {

  /**
    * Imports a file containing `source_id target_id edge_label`
    *
    * @param kPathIndex
    * @param filename
    * @param labels
    * @return
    */
  def buildBaseIndex(kPathIndex: KPathIndex, filename: String, labels: List[String]): Long = {
    var lines = 0

    val fileStream = getClass.getResourceAsStream(filename)
    val bufferedSource = Source.fromInputStream(fileStream)

    for (line <- bufferedSource.getLines) {
      val cols = line.split(" ") // .map(_.trim)
      val source: Long = cols(0).toLong
      val target: Long = cols(1).toLong
      val edgeLabel = cols(2)

      if(labels.contains(edgeLabel)) {
        val nodes = List(Node(source), Node(target))
        val pathId = PathIdStore.getPathIdByEdges(List(Edge(edgeLabel)))
        kPathIndex.insert(Path(pathId, nodes))
        lines += 1
      }
    }
    bufferedSource.close

    return lines
  }

}
