package bbk.curetti

import java.io.{BufferedWriter, FileWriter}

import bbk.curetti.conf.GraphConfig
import bbk.curetti.lubm.{LabelIdStore, NodeIdStore}

import scala.collection.mutable.ListBuffer
import scala.io.Source

object MainLUBM {
  val graphConfig = new GraphConfig()
  val dbName = "lubm"

  case class GraphEdge(source: Long, target: Long, edge: Long)

  def main(args: Array[String]) {
    val lubmInput = graphConfig.envOrElseConfig(s"datasets.$dbName.inputData")
    parse(lubmInput)
  }

  def parse(filename: String) = {

    val fileStream = getClass.getResourceAsStream(filename)
    val bufferedSource = Source.fromInputStream(fileStream)

    var results = ListBuffer[GraphEdge]()
    for (line <- bufferedSource.getLines) {
      val cols = line.split(",").map(_.trim)
      val source = NodeIdStore.getNodeId(cols(0))
      val target = NodeIdStore.getNodeId(cols(1))
      val edgeId = LabelIdStore.getLabelId(cols(2))
      results += GraphEdge(source, target, edgeId)
    }
    bufferedSource.close
    printOutput(results)
    LabelIdStore.printAllLabels()
    println(s"Number of nodes: ${NodeIdStore.getSize}")
  }

  def printOutput(results: Seq[GraphEdge]) = {
    val outputFileName = s"src/main/resources/datasets/lubm/data.csv"
    val outWriter = new BufferedWriter(new FileWriter(outputFileName))
    results.foreach { result =>
      val line = s"${result.source} ${result.target} ${result.edge}\n"
      outWriter.write(line)
    }
    outWriter.close()
  }

//  def createLine(result: GraphEdge): Array[String] = {
//    Array(
//      result.source.toString,
//      result.target.toString,
//      result.edge.toString)
//  }

}
