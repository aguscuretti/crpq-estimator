package bbk.curetti.kpathtable

trait KPathTable {

  def insert(id: Long, stats: PathIdStats): Unit
  def search(pathId: Long): PathIdStats
}

case class PathIdStats(sources: Set[Long], targets: Set[Long])


