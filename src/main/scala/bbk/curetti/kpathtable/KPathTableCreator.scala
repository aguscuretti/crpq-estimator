package bbk.curetti.kpathtable

import bbk.curetti.kpathindex.KPathIndex
import bbk.curetti.model.graph.{Path, PathPrefix}
import bbk.curetti.model.query.CRPQ
import bbk.curetti.sampling.PathReservoirSampler

object KPathTableExtender {

  /**
    * Adds an entry to the k-path table for the pathId of each RPQ conforming the CRPQ under estimation,
    * based on a 'sample' of the graph.
    * The sampling is not done directly on the input triplets, but on the indexed paths.
    *
    * @param kPathIndex the full k-path index created
    * @param kPathTable the table to create
    * @param samplingFactor double value between 0 and 1 that represents the percentage of paths from the index for
    *                       each pathId that will be used for the estimation
    * @param crpq the CRPQ that is being estimated, as an optimisation to just generate en entry in the table for
    *             pathIds that will intervene in the estimation (instead of keeping stats for all the pathIds, we just
    *             need the stats for the pathIds of each RPQ that conforms this CRPQ.
    *
    * @return kPathTable the created table
    */
  def run(kPathIndex: KPathIndex, kPathTable: KPathTable, samplingFactor: Double, crpq: Option[CRPQ] = None): Unit = {

    val pathIds = crpq.get.rpqs.map(_.pathId).distinct

    pathIds foreach { pathId =>
      val paths: Stream[Path] = kPathIndex.search(PathPrefix(pathId, List.empty))
      val reservoirSize = (paths.size * samplingFactor).toInt // check for hasDefiniteSize?
      val samplePaths = PathReservoirSampler.takeSample[Path](paths, reservoirSize)
      val sourceIds = samplePaths.map(_.nodes.head.id).toSet
      val targetIds = samplePaths.map(_.nodes.last.id).toSet
      kPathTable.insert(pathId, PathIdStats(sourceIds, targetIds))
    }
  }

}
