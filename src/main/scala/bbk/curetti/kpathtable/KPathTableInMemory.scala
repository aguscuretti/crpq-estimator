package bbk.curetti.kpathtable

import scala.collection.mutable

class KPathTableInMemory extends KPathTable{

  private val entries = mutable.Map[Long, PathIdStats]()

  override def insert(id: Long, stats: PathIdStats): Unit = {
    entries += (id -> stats)
  }

  override def search(pathId: Long): PathIdStats = {
    entries(pathId)
  }

}
