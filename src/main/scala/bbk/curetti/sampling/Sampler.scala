package bbk.curetti.sampling

import scala.reflect.ClassTag
import scala.util.Random

trait ReservoirSampler {

  def takeSample[A: ClassTag](input: Stream[A], reservoirSize: Int): Seq[A] = {
    val (sample, _) = SamplingUtils.reservoirSampleAndCount(input.iterator, reservoirSize)
    sample
  }
}

trait RandomSampler {

  def takeSample[T:ClassTag](list: Seq[T], n:Int, seed:Long) = {
    Random.shuffle(list).take(n)

    val r = new Random(seed)
    r.shuffle(list).take(n)
  }
}
