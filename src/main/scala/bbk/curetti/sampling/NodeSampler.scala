package bbk.curetti.sampling

import scala.reflect.ClassTag

class NodeSampler() extends ReservoirSampler {
  def takeSample[A: ClassTag](values: Seq[A], sampleSize: Int) =
    super.takeSample(values.toStream, sampleSize)
}

object NodeSampler {
  private val sampler = NodeSampler()
  def apply(): NodeSampler = sampler
}
