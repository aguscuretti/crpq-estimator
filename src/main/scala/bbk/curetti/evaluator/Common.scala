package bbk.curetti.evaluator

import bbk.curetti.model.query.RPQ
import grizzled.slf4j.Logging

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


case class JoinEvaluation(strategy: Int, rpq: RPQ)

trait Common extends Logging {

	object Strategy {
		val Disjunctive = -1
		val Independent = 0
		val SameSource = 1
		val SameTarget = 2
		val Concatenated = 3
	}

	var results = ListBuffer[RPQResult]()

	var sameSourceRpqs = mutable.Map[String, ListBuffer[RPQ]]()
	var sameTargetRpqs = mutable.Map[String, ListBuffer[RPQ]]()

	var isRpqConnectedMap = mutable.Map[RPQ, Boolean]()

	type JoinValue = Long
	type PossiblePreValues = Int
	type PossiblePostValues = Int

	type NodeId = String
	type SourceNodeId = String
	type SourceNodeValue = Long
	type TargetNodeId = String
	type PossibleValues = Int

	case class Estimation(cardinality: Int)

	def getProcessingStrategy(queryResult: QueryResult, previousRPQs: ListBuffer[RPQ], currentRPQ: RPQ): Int = {

		def isConnectingRPQ(previousRPQ: RPQ, currentRPQ: RPQ) = {

			if (previousRPQ.source == currentRPQ.source) {
				queryResult.updateSameSourceRpqs(ListBuffer(currentRPQ, previousRPQ))
				updateRpqsConnectedMap(List(currentRPQ, previousRPQ), true, queryResult)
				Strategy.SameSource
			}
			else if (previousRPQ.target == currentRPQ.target) {
				queryResult.updateSameTargetRpqs(ListBuffer(currentRPQ, previousRPQ))
				updateRpqsConnectedMap(List(currentRPQ, previousRPQ), true, queryResult)
				Strategy.SameTarget
			}
			else if (previousRPQ.target == currentRPQ.source || previousRPQ.source == currentRPQ.target) {
				queryResult.concatenatedPairs += ListBuffer(previousRPQ, currentRPQ)
				updateRpqsConnectedMap(List(currentRPQ, previousRPQ), true, queryResult)
				Strategy.Concatenated
			}
			else {
				updateRpqsConnectedMap(List(currentRPQ), false, queryResult)
				Strategy.Disjunctive
			}
		}

		previousRPQs.size match {
			case 0 =>
				Strategy.Independent
			case _ =>
				// TODO: how current matches against all previous RPQS starting with the first ones with more cardinality
				val possibleStrategy = for {
					previous <- previousRPQs
					result = isConnectingRPQ(previous, currentRPQ)
					if (result != Strategy.Disjunctive)
				} yield result

				if (possibleStrategy.size > 0)
					possibleStrategy.head
				else {
					updateRpqsConnectedMap(List(currentRPQ), false, queryResult) //insteads of false (rpqs.size == 1)?
					Strategy.Independent
				}
		}
	}

	private[evaluator] def updateRpqsConnectedMap(rpqs: List[RPQ], value: Boolean, queryResult: QueryResult) = {
		rpqs.foreach { rpq =>
			queryResult.isRpqConnectedMap.update(rpq, value)
		}
	}

	private[evaluator] def findNodeValuesAlongPathsFrom(node: String, values: List[Long],
		nodeValuesInBranch: ListBuffer[(NodeId, PossibleValues)]): Unit = {

		val nextResults = results.filter(res =>
			res.source == node && values.contains(res.sourceValue))

		nextResults.size match {
			case 0 =>
				nodeValuesInBranch.append((node, values.size))
			case _ =>
				nextResults.foreach { nextTargetNode =>
					val nextNode = nextTargetNode.target // z
				val currentTargetValues = nextTargetNode.targetValues
					currentTargetValues.foreach { currentTargetVal =>
						findNodeValuesAlongPathsFrom(nextNode, List(currentTargetVal), nodeValuesInBranch)
					}
				}
		}
	}

}
