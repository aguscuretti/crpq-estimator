package bbk.curetti.evaluator

import bbk.curetti.model.query.RPQ

import scala.collection.immutable
import scala.collection.mutable.ListBuffer

class QueryResultByStrategy extends Common {

  private def allRpqsAreConnected: Boolean = {
    val values = isRpqConnectedMap.valuesIterator.toSet
    if (values.forall(_ == true)) true else false
  }

  def evaluateSameSource() = {
    var addedSourceTrees: Long = 0
    sameSourceRpqs.foreach { sameSourceConjunction =>
      val sourceNode = sameSourceConjunction._1  // x                   r
    val rpqs = sameSourceConjunction._2        // x->y, x->a          r->s, r->t

      val resultsByEachSourceValue = ListBuffer[Long]()
      /**
        *  I need to get to the end possible values from x to all end nodes.
        *  Multiply those possibilities by source node value (x=5, x=1, x=6)
        *  Add results for each source node value
        */
      results
        .filter(_.source == sourceNode) // x->
        .groupBy(result => (result.source, result.sourceValue))  // (x,5)-> ;; (x,1)-> ;; (x,6)->  results grouped by sourceValue
        .map { (group: ((String, Long), ListBuffer[RPQResult])) => // ((x,5) -> rpqs)

        // here I'm inside each group (by sourceValue)
        val nextNodeWithValues = group._2.map { p => (p.target, p.targetValues) } // NEXT values: y -> List(6); a -> List(6)

        var chainByNode = ListBuffer[Long]()
        nextNodeWithValues.foreach { next =>
          val nextNode = next._1
          val currentNodeValues = next._2
          // lo sig para y,6 tiene que dar 3 (z puede tener 3 valores)
          val result = ListBuffer[(String, Int)]()
          findNodeValuesAlongPathsFrom(nextNode, currentNodeValues, result) // need to multiply for each currentNodeValue all the nextValuesFrom that node&value
        val nodeResult = result.map(_._2).reduceLeft(_ + _)
          chainByNode += nodeResult
        }

        val sourcesMultiplied = chainByNode.reduceLeft(_ * _) // if y,6 goes to any other node than z, then I need to multiply all the possible results by following node.
        resultsByEachSourceValue += sourcesMultiplied
        sourcesMultiplied
      }

      addedSourceTrees = resultsByEachSourceValue.reduceLeft(_ + _)
    }
    addedSourceTrees
  }

  def evaluateSameTarget() = {

    var addedValuesPerTargetForAll: Long = 0

    sameTargetRpqs.foreach { sameTargetConjunction =>
      val rpqs = sameTargetConjunction._2

      rpqs.map { rpq =>
        val outgoingValuesPerPostNode = results.filter(_.source == rpq.target.id)
          .groupBy(_.sourceValue)
          .map { joinValue => (joinValue._1, joinValue._2.map(_.targetValues.size).reduceLeft(_ * _))
          }.toList

        val incomingValuesBySource = results.filter(_.target == rpq.target.id)
          .groupBy(_.source)
          .map { resultForSource =>
            resultForSource._2.map(_.targetValues)
          }.map(_.flatten.toList.groupBy(identity).mapValues(_.size))

        val incomingPerSource = incomingValuesBySource.flatten.groupBy {
          _._1
        }.mapValues(_.map(_._2).reduceLeft(_ * _)).toList
        val inAndOut = (incomingPerSource ::: outgoingValuesPerPostNode)
        val resultPerSource = inAndOut.groupBy(_._1).mapValues(inAndOut => inAndOut.map(_._2).reduceLeft(_ * _))

        addedValuesPerTargetForAll = resultPerSource.map(_._2).reduceLeft(_ + _)
      }
    }
    addedValuesPerTargetForAll

  }

  def evaluateConcatenations(concats: ListBuffer[RPQ]) = {
    val possibleResultsFromAConcatenatedPair = ListBuffer[Long]()
    // instead of doing for each joining value, do for each source of a joiningPair (concatenated values)
    concats.foreach { concatenation =>
      val sourceJoinValue = concatenation.source.id

      val preCountsPerJoiningValue = results
        .filter(_.target == sourceJoinValue)
        .groupBy(result => (result.source, result.target))
        .map { group =>
          group._2
            .map(_.targetValues).flatten
            .groupBy(identity).mapValues(groups => groups.size)
        }.flatten.toList

      val targetJoinValue = concatenation.target.id
      val postValuesPerTargetJoinValue: immutable.Seq[(Long, Int)] = results
        .filter(_.source == sourceJoinValue)
        .map { joinValue => (joinValue.sourceValue, joinValue.targetValues.size) }
        .toList

      val sortedPre = preCountsPerJoiningValue.sortBy(_._1)
      val sortedPost = postValuesPerTargetJoinValue.sortBy(_._1)
      val zippedPreAndPost = sortedPre.map(_._2) zip sortedPost.map(_._2)
      val cardinalityOfThisConcatenation = zippedPreAndPost.foldLeft(0)((sum, pair) => sum + (pair._1 * pair._2))

      possibleResultsFromAConcatenatedPair += cardinalityOfThisConcatenation
    }

    possibleResultsFromAConcatenatedPair.reduceLeft(_ * _)
  }


}
