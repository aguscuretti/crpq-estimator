package bbk.curetti.evaluator

import bbk.curetti.kpathindex.KPathIndex
import bbk.curetti.model.graph.{Path, PathPrefix}
import bbk.curetti.model.query.{CRPQ, RPQ}
import grizzled.slf4j.Logging

import scala.collection.mutable.ListBuffer

case class Pair(source: Long, target: Long)

case class RPQResult(source: String, target: String, sourceValue: Long, var targetValues: List[Long]) {

  override def toString(): String = s"$source -> $target, ${String.valueOf(sourceValue)} -> ${targetValues.toString}"
}

case class NodeValue(node: String, value: Long, comingFrom: Option[Long] = None)

case class ConcatenatedRPQ(rpq: RPQ, var isProcessed: Boolean = false)

class CRPQCardinalityEvaluator(kPathIndex: KPathIndex, queryResult: QueryResult) extends Common with Logging {

  var rpqsToProcess = List[RPQ]()

  val concatenations = ListBuffer[ConcatenatedRPQ]()

  def calculateCardinality(crpq: CRPQ, restrictNodeValues: Map[String, Set[Long]]): BigInt = {
    val rpqs = crpq.rpqs
    rpqsToProcess = rpqs
    queryResult.rpqs = rpqs
    val sorted = sortByCardinality(rpqs)

    updateRpqsConnectedMap(rpqs, false, queryResult) // all false
    val alreadyProcessedRPQs = ListBuffer[RPQ]()

    var resultSet = ListBuffer[RPQResult]()
    sorted.foreach { nextRPQ =>
      // get how it joins with previous evaluated RPQs AAAAND this function puts together rpqs with sameSource, sameTarget, concatenations
      val strategy: Int = getProcessingStrategy(queryResult, alreadyProcessedRPQs, nextRPQ)

      //todo: check if still needed here the map updates
      strategy match {
        case Strategy.Independent if rpqs.size == 1 =>
          queryResult.isRpqConnectedMap.update(nextRPQ, true)

        case Strategy.SameSource | Strategy.SameTarget | Strategy.Concatenated =>
          queryResult.isRpqConnectedMap.update(nextRPQ, true)

        case Strategy.Disjunctive | Strategy.Independent if rpqs.size > 1 =>
          queryResult.isRpqConnectedMap.update(nextRPQ, false)
      }

      // calculate the join and add the results to Query Result object
      resultSet = calculateJoinWithQueryResult(nextRPQ, strategy, restrictNodeValues)
      if (resultSet.isEmpty){
        println(s"Empty Results after processing ${RPQ.prettyPrint(nextRPQ)}")
      }
      val xynodes: List[(NodeId, NodeId)] = resultSet.map(res => (res.source, res.target)).distinct.toList
      println(s"ResultSet has entries for ${xynodes}")
      //      val totalPossibleValuesPerNode: Seq[(String, Int)] = getTotalPossibleValuesPerNodeSoFar(queryResult, xynodes)
      //      totalPossibleValuesPerNode.foreach(rec => println(s"node ${rec._1}: ${rec._2}"))
      alreadyProcessedRPQs += nextRPQ // used for getting the joining strategy!!!
    }
    println(s"\n")
    queryResult.cardinality
  }

  def getTotalPossibleValuesPerNodeSoFar(queryResult: QueryResult, evaluatedSoFar: List[(NodeId, NodeId)]) = {
    val possibleValues = queryResult.possibleValues
    val nodesSoFar = evaluatedSoFar.foldLeft(ListBuffer[String]()) { (acu, tuple) => acu += tuple._1; acu += tuple._2 }.toList.distinct
    nodesSoFar.map { nodeProcessed =>
      val nodeValues = possibleValues.filter(entry => entry.node == nodeProcessed).map(_.value).toList.distinct
      val totalNodeValues = nodeValues.size
      (nodeProcessed, totalNodeValues)
    }

  }

  def sortByCardinality(rpqs: List[RPQ]): List[RPQ] = rpqs.sortWith(sortByPathCardinality)

  def sortByPathCardinality(rpq1: RPQ, rpq2: RPQ) = {
    val totalPaths1 = kPathIndex.search(PathPrefix(rpq1.pathId, List.empty)).iterator.count(_ => true)
    val totalPaths2 = kPathIndex.search(PathPrefix(rpq2.pathId, List.empty)).iterator.count(_ => true)
    totalPaths1 < totalPaths2
  }

  def getRPQsWithSameSource(rpqs: List[RPQ]): List[RPQ] = {
    rpqs.groupBy(_.source.id).filter(group => group._2.size > 1).flatMap(x => x._2).toList
  }

  def getRPQsWithSameTarget(rpqs: List[RPQ]): List[RPQ] = {
    rpqs.groupBy(_.target.id).filter(group => group._2.size > 1).flatMap(x => x._2).toList
  }

  def getConcatenatedRPQs(rpqs: List[RPQ]): List[RPQ] = {
    val concatenated = scala.collection.mutable.ListBuffer[RPQ]()
    rpqs.foreach { rpq =>
      val possibleConcatRPQs = rpqs.filter(_.source.id == rpq.target.id)
      if (possibleConcatRPQs.size > 0) {
        concatenated += rpq
        possibleConcatRPQs.foreach(r => concatenated += r)
      }
    }
    concatenated.toList
  }

  def getPairs(rpq: RPQ): List[Pair] = {
    val paths: Seq[Path] = kPathIndex.search(PathPrefix(rpq.pathId, List.empty))
    paths
      .map(path => Pair(path.nodes.head.id, path.nodes.last.id))
      .toList
  }

  def getPairsWithRestrictedValues(rpq: RPQ, restrictedValues: Map[String, Set[Long]]): List[Pair] = {
    val allPaths: Seq[Path] = kPathIndex.search(PathPrefix(rpq.pathId, List.empty))

    val sourceNodeId = rpq.source.id
    val targetNodeId = rpq.target.id
    val filteredPaths = ListBuffer[Path]()
    if (!restrictedValues.isEmpty) {
      if (restrictedValues.contains(sourceNodeId)) {
        val onlyPossibleFirstNodeValues = restrictedValues(sourceNodeId)
        val onlyAdd = allPaths.filter(path => onlyPossibleFirstNodeValues.contains(path.nodes.head.id))
        filteredPaths ++= onlyAdd
      } else if (restrictedValues.contains(targetNodeId)) {
        val onlyPossibleLastNodeValues = restrictedValues(targetNodeId)
        filteredPaths ++= allPaths.filter(path => onlyPossibleLastNodeValues.contains(path.nodes.last.id))
      }
    }

    val pathsToReturn = if (filteredPaths.isEmpty) {
      allPaths
    } else {
      filteredPaths
    }

    val answerPairs = pathsToReturn.map(path => Pair(path.nodes.head.id, path.nodes.last.id)).toList
    answerPairs
  }


  def calculateJoinWithQueryResult(next: RPQ, strategy: Int, restrictedValues: Map[String, Set[Long]]): ListBuffer[RPQResult] = {
    calculateJoin(next, next.source.id, next.target.id, getPairsWithRestrictedValues(next, restrictedValues), strategy)
  }

  def removeConnectionsBackwards(queryResult: QueryResult, deletedRPQs: List[RPQResult]): Unit = {

    val sourceNodeAndValue = deletedRPQs.map(rpq => (rpq.source, rpq.sourceValue))

    val toDelWhereNodeIsSource = queryResult.results.filter { res => sourceNodeAndValue.contains((res.source, res.sourceValue)) }.toList
    queryResult.removeResults(toDelWhereNodeIsSource)

    val rpqsWhereNodeIsTarget = queryResult.results.filter(res => sourceNodeAndValue.map(_._1).contains(res.target))

    if (rpqsWhereNodeIsTarget.size > 0) {
      val rpqToDeleteCauseOnlyTargetMustBeDeleted = rpqsWhereNodeIsTarget.filter { res =>
        val value = sourceNodeAndValue.find(pair => pair._1 == res.target).get._2
        res.targetValues == List(value)
      }.toList
      queryResult.removeResults(rpqToDeleteCauseOnlyTargetMustBeDeleted)

      // delete from targets:
      val rpqsWithTargetsToBeFiltered = queryResult.results.filter(res => sourceNodeAndValue.map(_._1).contains(res.target))
      rpqsWithTargetsToBeFiltered.foreach { rpq =>
        val valuesToDelete = sourceNodeAndValue.filter(_._1 == rpq.target).map(_._2)
        queryResult.clearInAndOutForNodeValue(rpq.target, valuesToDelete)
      }

      //continue going backwards:
      if (rpqToDeleteCauseOnlyTargetMustBeDeleted.size > 0) {
        removeConnectionsBackwards(queryResult, rpqToDeleteCauseOnlyTargetMustBeDeleted)
      }
    }
  }


  def getRightSideRPQOfPair(concatenatedPair: ListBuffer[RPQ]): RPQ = {
    val first = concatenatedPair.head
    val second = concatenatedPair.last
    if (first.source.id == second.target.id)
      first
    else // (first.target.id == second.source.id)
      second
  }

  def calculateJoin(currentRPQ: RPQ, source: String, target: String, answerPairs: List[Pair], strategy: Int): ListBuffer[RPQResult] = {

    implicit class FilterHelper[A](l: List[A]) {
      def ifFilter(cond: Boolean, f: A => Boolean) = {
        if (cond) l.filter(f) else l
      }
    }

    strategy match {
      case Strategy.SameSource =>
        val joinValues = queryResult.getValuesFor(source)
        val conjunctionResult = answerPairs
          .filter(pair => joinValues.contains(pair.source)) // we only consider the pairs that match the 'existing result' since we need to satisfy ALL individual RPQs
          .groupBy(_.source)
          .map(group => RPQResult(source, target, group._1, group._2.map(_.target).distinct))
          .toList
        val conjunctionValues = conjunctionResult.map(_.sourceValue)
        val toRemove = queryResult.results.filter { result => !conjunctionValues.contains(result.sourceValue) }.toList
        queryResult.removeResults(toRemove)
        queryResult.addResults(conjunctionResult)
        queryResult.calculateCardinality(Strategy.SameSource)

      case Strategy.SameTarget =>

        val joinValues = queryResult.getValuesFor(target)
        val conjunctionResult = answerPairs
          .filter(pair => joinValues.contains(pair.target)) // only consider the pairs that match the 'existing result'
          .groupBy(_.source)
          .map(group => RPQResult(source, target, group._1, group._2.map(_.target).distinct))
          .toList

        if(conjunctionResult.size > 0) {
          val targetValues = conjunctionResult.map(_.targetValues).flatten.distinct

          // remove results where target node == target and targetValue is not any of conjunction values
          val removeResultsWhereTargetsIsNoneOfConjunctionTargetValues =
            queryResult.results
              .filter(result =>
                result.target == target &&
                  !result.targetValues.exists(targetValues.contains)).toList

          queryResult.removeResults(removeResultsWhereTargetsIsNoneOfConjunctionTargetValues)

          queryResult.cleanTargets(targetValues, target)
          queryResult.addResults(conjunctionResult)
          queryResult.calculateCardinality(strategy = Strategy.SameTarget, target = Some(target))
        } else List.empty

      case Strategy.Concatenated =>

        concatenations += (ConcatenatedRPQ(currentRPQ))

        val concatenatedPairs = queryResult.concatenatedPairs.filter(pair => pair.contains(currentRPQ))
        concatenatedPairs.foreach { concatenatedPair =>
          val rightRpq = getRightSideRPQOfPair(concatenatedPair)
          val joinNodeId = rightRpq.source.id
          val leftRpq = concatenatedPair.toList.filterNot(rpq => rpq.source.id == joinNodeId).head

          val joinValues = queryResult.getValuesFor(joinNodeId)

          val filteredPairs: List[Pair] = Option(joinValues.size) match {
            case Some(size) if size > 0 && (currentRPQ == rightRpq) =>
              val destinationValues = queryResult.getValuesFor(target)
              val condition = destinationValues.size > 0

              def filterTargetValue(pair: Pair): Boolean = {
                destinationValues.contains(pair.target)
              }
              answerPairs
                .filter(pair => joinValues.contains(pair.source))
                .ifFilter(condition, filterTargetValue) // to add only pairs that match the current possible values of the target node

            case Some(size) if size > 0 && (currentRPQ == leftRpq) =>
              val previousValuesForSource = queryResult.getValuesFor(source)
              val condition = previousValuesForSource.size > 0
              def filterSourceValue(pair: Pair): Boolean = {
                previousValuesForSource.contains(pair.source)
              }
              answerPairs
                .filter(pair => joinValues.contains(pair.target))
                .ifFilter(condition, filterSourceValue)  // to add only pairs that match current possible values of the source node

            case Some(size) if size == 0 => answerPairs.filter(pair => joinValues.contains(pair.source))
          }

          val conjunctionResult: List[RPQResult] = createRPQResults(source, target, filteredPairs)

          if (conjunctionResult.size == 0){
            println(s"rightRPQ: ${RPQ.prettyPrint(rightRpq)}")
            println(s"leftRPQ: ${RPQ.prettyPrint(leftRpq)}")
            println(s"conjunction result is 0 (s,t)$source,$target with ()$joinNodeId,${rightRpq.target.id}")
          }
          require(conjunctionResult.size > 0)

          val resultSourceValues = conjunctionResult.map(_.sourceValue).distinct
          val resultTargetValues = conjunctionResult.map(_.targetValues).flatten.distinct

          // remove results that do not connect anymore TO the conjunction source values
          val notConnectedToResultSources = queryResult.results
            .filter(_.target == source)
            .filter(result => !result.targetValues.exists(resultSourceValues.contains)).toList
          queryResult.removeResults(notConnectedToResultSources)
          // and drop recursively all the connections from the source of those values
          removeConnectionsBackwards(queryResult, notConnectedToResultSources)

          // remove results that do not connect anymore FROM the conjunctive target values
          // there will be nothing to remove if there are no pre-existing values for the node values involved
          val notConnectedFromResultTargets = queryResult.results
            .filter(_.source == target)
            .filter(result => !resultTargetValues.contains(result.sourceValue)).toList
          queryResult.removeResults(notConnectedFromResultTargets)
          //            removeConnectionsForwards(queryResult, notConnectedFromResultTargets)

          // remove results that do not connect anymore TO the conjunction target values
          val notConnectedToResultTargets = queryResult.results
            .filter(_.target == target)
            .filter(result => result.targetValues.intersect(resultTargetValues).size == 0).toList
          queryResult.removeResults(notConnectedToResultTargets)

          // remove from existing results extra values in targetValues that are not conjunctive ones
          queryResult.filterTargetValues(source, resultSourceValues)
          queryResult.filterSourceValues(target, resultTargetValues)

          queryResult.addResults(conjunctionResult)
        }
        //                println(s"Evaluating now...  ${RPQ.prettyPrint(currentRPQ)} Concatenations: (${concatenations.size}) ${concatenations.map(concat => RPQ.prettyPrint(concat.rpq))}")
        queryResult.calculateCardinality(
          strategy = Strategy.Concatenated,
          source = Some(source),
          target = Some(target),
          concatenations = concatenations
        )


      case Strategy.Independent =>
        val result = answerPairs
          .groupBy(_.source)
          .map(group => RPQResult(source, target, group._1, group._2.map(_.target).distinct))
          .toList

        queryResult.addResults(result)
        queryResult.calculateCardinality(strategy)
    }
    queryResult.results
  }

  private def createRPQResults(source: String, target: String, filteredPairs: List[Pair]) = {
    val conjunctionResult = filteredPairs
      .groupBy(_.source)
      .map(group => RPQResult(source, target, group._1, group._2.map(_.target).distinct))
      .toList
    conjunctionResult
  }


}
