package bbk.curetti.evaluator

import bbk.curetti.model.query.RPQ
import grizzled.slf4j.Logging

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Case class to store the possible values per incoming or outgoing edge for a join value.
  *
  * @param joinValue               the join value
  * @param connectedPossibleValues the number of possible values that the connected node can have
  * @param connectedNodeId         the id of the connected node to the node assuming the join value
  */
case class ConnectedPossibleValues(joinValue: Long, connectedPossibleValues: Int, connectedNodeId: String)


case class ConnectedNodeValue(joinValue: Long, connectedNodeValue: Int, connectedNodeId: String)


class QueryResult extends Common with Logging {

	var concatenatedPairs = ListBuffer[ListBuffer[RPQ]]()
	var possibleValues = Set[NodeValue]() // (Node -> (Value, Set(ComingFrom))

	var cardinality: BigInt = 0

	var rpqs = List[RPQ]()

	def getNodes() = possibleValues.map(_.node)

	def addResults(rpqResults: List[RPQResult]) = {
		results ++= rpqResults
		possibleValues ++= extractNodeValues(rpqResults)
	}

	def removeResults(rpqResults: List[RPQResult]) = {
		results --= rpqResults
		possibleValues --= extractNodeValues(rpqResults)
	}

	def updateSameTargetRpqs(rpqsToAdd: ListBuffer[RPQ]) = {
		val target = rpqsToAdd.head.target.id
		sameTargetRpqs.get(target) match {
			case Some(xs: ListBuffer[RPQ]) => sameTargetRpqs.updated(target, xs ++= rpqsToAdd)
			case None => sameTargetRpqs += (target -> rpqsToAdd)
		}
	}

	def updateSameSourceRpqs(rpqsToAdd: ListBuffer[RPQ]) = {
		val source = rpqsToAdd.head.source.id
		sameSourceRpqs.get(source) match {
			case Some(xs: ListBuffer[RPQ]) => sameSourceRpqs.updated(source, xs ++= rpqsToAdd)
			case None => sameSourceRpqs += (source -> rpqsToAdd)
		}
	}

	def findUltimatePreSource(fromNode: String): Unit = {
		val preNodes = rpqs.filter(rpq => rpq.target.id == fromNode).map(_.source.id)
		if (preNodes.size == 1) {
			preNodes.foreach(preNode => findUltimatePreSource(preNode))
		}
	}

	def sort(rpqs: List[RPQ]): List[RPQ] = {
		val sortedBySource = rpqs.sortWith(sortBySourceStrategy)
		sortedBySource
	}

	def sortBySourceStrategy(rpq1: RPQ, rpq2: RPQ) = {
		val totalSources1 = rpqs.filter(_.source.id == rpq1.source.id).size
		val totalSources2 = rpqs.filter(_.source.id == rpq2.source.id).size
		totalSources1 > totalSources2
	}

	def groupResultsByTargetValue(resultsToGroup: ListBuffer[RPQResult],
	  multiplierFactors: Map[(SourceNodeId, SourceNodeValue), (NodeId, PossibleValues)]
	): List[ConnectedPossibleValues] = {

		val connectedNodesWithPossibleValues = ListBuffer[ConnectedPossibleValues]()

		val uniqueTargetJoinValues = resultsToGroup.map(_.targetValues).flatten.distinct
		// for each join value
		uniqueTargetJoinValues.foreach { (joinValue: JoinValue) =>
			val concatenatedResults = resultsToGroup.filter(_.targetValues.contains(joinValue))
			concatenatedResults.foreach { result =>
				val key = (result.source, result.sourceValue)
				if (multiplierFactors.get(key).isDefined) {
					val possibleValuesFromOtherConcatenation: (NodeId, PossibleValues) = multiplierFactors(key)
					connectedNodesWithPossibleValues += (
					  ConnectedPossibleValues(
						  joinValue = joinValue,
						  connectedPossibleValues = possibleValuesFromOtherConcatenation._2,
						  connectedNodeId = "")
					  )
				} else {
					if (connectedNodesWithPossibleValues.filter(_.joinValue == joinValue).size == 0) // only add once if there are no bifurcations from pre nodes
						connectedNodesWithPossibleValues += (
						  ConnectedPossibleValues(
							  joinValue = joinValue,
							  connectedPossibleValues = concatenatedResults.size,
							  connectedNodeId = "")
						  )
				}
			}
		}
		connectedNodesWithPossibleValues.toList
	}

	def sumByKeys(tuples: List[(String, Int)]): List[(String, Int)] = {
		tuples.groupBy(_._1).mapValues(_.unzip._2.sum).toList
	}

	def findFirstSource(id: String): String = {
		val possibleResult = results.find(result => result.target == id)
		possibleResult match {
			case None => id
			case Some(rpq) => findFirstSource(rpq.source)
		}
	}

	def findLastTarget(id: String): String = {
		val possibleResult = results.find(result => result.source == id)
		possibleResult match {
			case None => id
			case Some(rpq) => findFirstSource(rpq.target)
		}
	}

	def concatenationsHaveAllSameSourceOrSameTarget(concats: ListBuffer[ConcatenatedRPQ]): Boolean = {
		val firstSources = ListBuffer[String]()
		val lastTargets = ListBuffer[String]()
		concats.foreach { concat =>
			firstSources += findFirstSource(concat.rpq.source.id)
			lastTargets += findLastTarget(concat.rpq.target.id)
		}
		if (firstSources.distinct.size == 1 || lastTargets.distinct.size == 1) true
		else false
	}

	def isLastConcatenation(concatenations: ListBuffer[ConcatenatedRPQ]): Boolean = {
		var isLastFlagPerConcat = ListBuffer[Boolean]()
		concatenations.foreach { concat =>
			val isLast = rpqs.filter(res => res.source == concat.rpq.target).size == 0
			isLastFlagPerConcat += isLast
		}
		isLastFlagPerConcat.forall(_ == true)
	}

	def updateMap[Source, Target, Result](map: Map[(Source, Target), Result], key: (Source, Target), value: Result) =
		map + ((key, map.getOrElse(key, value)))

	def removeFromMap[Source, Target, Result](map: Map[(Source, Target), Result], key: (Source, Target)) =
		map - key

	def calculateCardinality(strategy: Int,
	  source: Option[String] = None,
	  target: Option[String] = None,
	  concatenations: ListBuffer[ConcatenatedRPQ] = ListBuffer()): BigInt = {

		cardinality = strategy match {

			case Strategy.SameSource | Strategy.Independent =>
				if (results.size > 0) {
					val r = results
					  .groupBy(result => (result.source, result.sourceValue))
					  .map { group =>
						  group._2
							.map(_.targetValues.size)
							.reduceLeft(_ * _) // multiply number of nodes per target
					  }
					val all = r.reduceLeft(_ + _) // add all the results for each source
					println(s"Card after processing SS/I: $all")
					all
				}
				else {
					println(s"results are empty.")
					0
				}


			case Strategy.SameTarget =>
				//even if it is same target, that target can have other relationships outgoing.
				//multiply how many different incoming per source node *  how many different outputs per post-concatenated node
				//1.the outgoing from the node, assuming unique set in targetValues

				val outgoingValuesPerPostNode = results.filter(_.source == target.get)
				  .groupBy(_.sourceValue)
				  .map { joinValue => (joinValue._1, joinValue._2.map(_.targetValues.size).reduceLeft(_ * _))
				  }.toList

				val incomingValuesBySource = results.filter(_.target == target.get)
				  .groupBy(_.source)
				  .map { resultForSource =>
					  resultForSource._2.map(_.targetValues)
				  }.map(_.flatten.toList.groupBy(identity).mapValues(_.size))

				val incomingPerSource = incomingValuesBySource.flatten.groupBy {
					_._1
				}.mapValues(_.map(_._2).reduceLeft(_ * _)).toList
				val inAndOut = (incomingPerSource ::: outgoingValuesPerPostNode)
				val resultPerSource = inAndOut.groupBy(_._1).mapValues(inAndOut => inAndOut.map(_._2).reduceLeft(_ * _))

				val addedValuesPerSourceForAll = resultPerSource.map(_._2).reduceLeft(_ + _)
				println(s"Card after processing ST: $addedValuesPerSourceForAll")
				addedValuesPerSourceForAll


			case Strategy.Concatenated =>

				val resultsPerJoinValue = Map[(SourceNodeId, TargetNodeId), Int]()

				concatenations.foreach { concatenation =>
					val rpq = concatenation.rpq
					val sourceNodeId = rpq.source.id
					val targetNodeId = rpq.target.id

					val incomingResults = resultsPerIncomingEdge(sourceNodeId)
					val outgoingResults = resultsPerOutgoingEdge(sourceNodeId)

					if (incomingResults.size > 0 || outgoingResults.size > 0) {
						val currentSourcesInResults = results.map(_.source).distinct.toList

						val currentConcatCardinality = currentSourcesInResults.contains(target.get) match {
							case true =>
								val outgoingConnectedNodes = results
								  .filter(result => result.source == sourceNodeId)
								  .flatMap { joinResult =>
									  joinResult.targetValues.map { targetValue =>
										  ConnectedNodeValue(
											  joinValue = joinResult.sourceValue,
											  connectedNodeValue = targetValue.toInt,
											  connectedNodeId = joinResult.target)
									  }
								  }.toList
								concatenationCardinality(incomingResults, outgoingConnectedNodes, targetNodeId)

							case false =>
								val sortedIncoming = incomingResults.sortBy(_.joinValue)
								val sortedOutgoing = outgoingResults.sortBy(_.joinValue)
								val (preMap, postMap) = keepOnlyConnectedValuesToJoinValues(sortedIncoming, sortedOutgoing)
								val zippedPreAndPost = ListBuffer[(PossiblePreValues, PossiblePostValues)]()
								preMap.foreach { preEntry =>
									val postPossibleValues = postMap
									  .filter(_.joinValue == preEntry.joinValue)
									  .map(_.connectedPossibleValues).head
									zippedPreAndPost += ((preEntry.connectedPossibleValues, postPossibleValues))
								}
								zippedPreAndPost.foldLeft(0)((sum, pair) => sum + (pair._1 * pair._2))
						}

						val targetsOfPreviousConcatenations = concatenations.map(_.rpq).map(_.target.id)

						if (targetsOfPreviousConcatenations.contains(sourceNodeId)) {
							val concatenationToUpdate = resultsPerJoinValue.find { case ((sourceId, targetId), card) =>
								targetId == sourceNodeId
							}
							if (concatenationToUpdate.isDefined) {
								val originalSource = concatenationToUpdate.get._1._1
								removeFromMap(resultsPerJoinValue, (originalSource, sourceNodeId))
								updateMap(resultsPerJoinValue, (originalSource, targetNodeId), currentConcatCardinality)
							}
						} else {
							updateMap(resultsPerJoinValue, (sourceNodeId, targetNodeId), currentConcatCardinality)
						}
					}
					concatenation.isProcessed = true
				}

				if (isLastConcatenation(concatenations) &&
				  concatenationsHaveAllSameSourceOrSameTarget(concatenations)) {
					resultsPerJoinValue.head._2
				} else {
					resultsPerJoinValue.values.reduceLeft(_ * _)
				}
			case Strategy.Disjunctive => 0
		}
		cardinality
	}


	def resultsPerIncomingEdge(nodeId: String): List[ConnectedPossibleValues] = {
		def updateMap[SOURCEID, SOURCEVALUE, TARGETID, POSSIBLEVALUES](
		  map: Map[(SOURCEID, SOURCEVALUE), (TARGETID, POSSIBLEVALUES)],
		  key: (SOURCEID, SOURCEVALUE),
		  value: (TARGETID, POSSIBLEVALUES)) =
			map + ((key, map.getOrElse(key, value)))

		val resultsToSource = results.filter(_.target == nodeId)
		val connectedValues = resultsToSource.map(_.source).distinct
		val otherResultsFromConnectedValues = results.filter(res => connectedValues.contains(res.source) && res.target != nodeId)
		val sourceToTargets = Map[(SourceNodeId, SourceNodeValue), (TargetNodeId, PossibleValues)]()

		resultsToSource.foreach { preJoinValue =>
			val source: String = preJoinValue.source
			val sourceValue: Long = preJoinValue.sourceValue
			val concatenatedResults = otherResultsFromConnectedValues.filter(res => res.source == source && res.sourceValue == sourceValue)
			concatenatedResults.foreach { concat =>
				val result = ListBuffer[(String, Int)]()
				findNodeValuesAlongPathsFrom(concat.target, concat.targetValues, result)
				val addedResult = sumByKeys(result.toList)
				require(addedResult.size == 1)
				updateMap(sourceToTargets, (source, sourceValue), (concat.target, addedResult.head._2))
			}
		}
		val resultsGroupedByTargetValue = groupResultsByTargetValue(resultsToSource, sourceToTargets)
		resultsGroupedByTargetValue
	}

	def resultsPerOutgoingEdge(nodeId: String): List[ConnectedPossibleValues] = {
		results
		  .filter(result => result.source == nodeId)
		  .map { joinResult =>
			  val result = ListBuffer[(String, Int)]()
			  findNodeValuesAlongPathsFrom(joinResult.target, joinResult.targetValues, result)
			  val addedResult = sumByKeys(result.toList)
			  require(addedResult.size == 1)
			  ConnectedPossibleValues(joinValue = joinResult.sourceValue, connectedPossibleValues = addedResult.head._2, connectedNodeId = joinResult.target)
		  }.toList
	}

	def concatenationCardinality(incomingPossibleValues: List[ConnectedPossibleValues],
	  outgoingConnectedNodes: List[ConnectedNodeValue],
	  targetNodeId: TargetNodeId): Int = {
		val nextJoiningValues = outgoingConnectedNodes.map(_.connectedNodeValue)

		val outgoingConnections = nextJoiningValues.map { nextConnectedValue =>
			val actualEndNodes = results.find(res => res.source == targetNodeId && res.sourceValue == nextConnectedValue).map(_.targetValues.size)
			ConnectedPossibleValues(joinValue = nextConnectedValue, actualEndNodes.getOrElse(0), connectedNodeId = targetNodeId)
		}
		val resultsForAllJoiningValues = ListBuffer[Int]()
		val countForJoiningValuePerEdge = ListBuffer[Int]()

		(incomingPossibleValues.size, outgoingConnections.size) match {
			case (inSize, _) if inSize > 0 =>
				val sortedIncoming = incomingPossibleValues.sortBy(_.joinValue)

				sortedIncoming.foreach { incomingConnection =>
					val joiningValue = incomingConnection.joinValue

					val nextValues = outgoingConnectedNodes.filter(_.joinValue == joiningValue).map(_.connectedNodeValue)

					nextValues.foreach { joinValue =>
						val outgoingValues = outgoingConnections.find(_.joinValue == joinValue).get.connectedPossibleValues
						val incomingValues = incomingConnection.connectedPossibleValues
						countForJoiningValuePerEdge += (incomingValues * outgoingValues)
					}
					val resultsForJoiningValue = countForJoiningValuePerEdge.reduceLeft(_ + _)
					resultsForAllJoiningValues += resultsForJoiningValue
				}

				resultsForAllJoiningValues.reduceLeft(_ * _)

			case (_, outSize) if outSize > 0 =>
				outgoingConnections.map { outgoingValues =>
					resultsForAllJoiningValues += outgoingValues.connectedPossibleValues
				}
				resultsForAllJoiningValues.reduceLeft(_ + _)
		}
	}

	def keepOnlyConnectedValuesToJoinValues(prePairs: Seq[ConnectedPossibleValues],
	  postPairs: Seq[ConnectedPossibleValues]) = {
		val preLen = prePairs.size
		val postLen = postPairs.size
		if (preLen != postLen) {
			if (preLen > postLen) {
				val filteredPrePairs = prePairs.filter(pair => postPairs.map(_.joinValue).contains(pair.joinValue))
				(filteredPrePairs, postPairs)
			} else {
				val filteredPostPairs = postPairs.filter(pair => prePairs.map(_.joinValue).contains(pair.joinValue))
				(prePairs, filteredPostPairs)
			}
		} else (prePairs, postPairs)
	}

	def findConcatenationToUpdateCardinalityWhereTargetIs(concatenations: Seq[RPQ], target: String): Option[RPQ] = {
		concatenations.find(_.target.id == target)
	}

	def extractNodeValues(rpqResults: List[RPQResult]): ListBuffer[NodeValue] = {
		val result = ListBuffer[NodeValue]()
		rpqResults.map { rpqResult =>
			result += NodeValue(rpqResult.source, rpqResult.sourceValue)
			result ++= rpqResult.targetValues.map(target => NodeValue(rpqResult.target, target, Some(rpqResult.sourceValue)))
		}
		result
	}

	def cleanTargets(targets: List[Long], targetNode: String) = {
		results.filter(_.target == targetNode).foreach { result =>
			result.targetValues = result.targetValues filter targets.contains
		}
	}

	def filterTargetValues(nodeValue: String, keepValues: List[Long]) = {
		results
		  .filter(_.target == nodeValue)
		  .map { result =>
			  result.targetValues = result.targetValues filter keepValues.contains
		  }
	}


	def clearInAndOutForNodeValue(nodeValue: String, deleteValues: List[Long]) = {
		//clear post connections
		results
		  .filter(node => node.target == nodeValue)
		  .map(result =>
			  result.targetValues = result.targetValues filterNot deleteValues.contains
		  )

		// clear any pre connections
		results
		  .filter(node => node.source == nodeValue)
		  .filterNot(node => deleteValues.contains(node.sourceValue))

	}

	def filterSourceValues(nodeValue: String, keepValues: List[Long]) = {
		results
		  .filter(_.source == nodeValue)
		  .filter(node => keepValues.contains(node.sourceValue))
	}

	def getValuesFor(node: String): List[Long] = possibleValues.filter(_.node == node).map(_.value).toList

	// output: List[(joiningValue, numIncomingValues)]
	def mapNumberOfSourcesToEachTargetIn(resultsToProcess: ListBuffer[RPQResult], interestedTargets: List[String]): List[(Long, Int)] = {
		// input is:
		// t->x, 2->List(5)
		// t->x, 3->List(6)
		// output expected is: (5,1), (6, 1)
		val incomingByTarget = mutable.Map[Long, Int]()
		resultsToProcess
		  .filter(res => interestedTargets.contains(res.source))
		  .map { result =>
			  result.targetValues.foreach { target =>
				  addOrUpdate(incomingByTarget, target)
			  }
		  }
		incomingByTarget.toList
	}

	def addOrUpdate(map: mutable.Map[Long, Int], key: Long) = {
		map.get(key) match {
			case Some(value) => map.update(key, value + 1)
			case None => map += (key -> 1)
		}
	}
}
