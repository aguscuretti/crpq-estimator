package bbk.curetti.estimator

trait EstimatorCommon {

  type NodeId = String
  type FromNodeId = String
  type ToNodeId = String

  type PathId = Long

  type PossibleValues = Set[Long]

}
