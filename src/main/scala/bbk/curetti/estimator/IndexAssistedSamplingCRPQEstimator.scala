package bbk.curetti.estimator

import bbk.curetti.evaluator.{CRPQCardinalityEvaluator, QueryResult}
import bbk.curetti.kpathindex.KPathIndexInMemory
import bbk.curetti.kpathtable.KPathTableInMemory
import bbk.curetti.model.query.CRPQ
import bbk.curetti.sampling.NodeRandomSampler


// !! Make sure kPathTable was created using sampling factor 1 (so to use all paths in index) !!
class IndexAssistedSamplingCRPQEstimator(kPathTable: KPathTableInMemory,
                                         kPathIndex: KPathIndexInMemory,
                                         samplingFactor: Double)
  extends AbstractCRPQCardinalityEstimator(kPathTable) {

  override def estimateCardinality(crpq: CRPQ): BigInt = {

    val nodeIds = getNodeIdsFromCrpq(crpq)
    val estimationPerNode = nodeIds.map { currentNodeId =>
      val initialNodeIncomingPathIds = getIncomingPathIdsFor(currentNodeId, crpq)
      val initialNodeOutgoingPathIds = getOutgoingPathIdsFor(currentNodeId, crpq)
      val nodeValues = getNodeValues(initialNodeIncomingPathIds, initialNodeOutgoingPathIds)
      val sampleSize = (samplingFactor * nodeValues.size).toInt
      val sampleNodes = NodeRandomSampler.takeSample(nodeValues.toSeq, sampleSize, 2)

      val evaluator = new CRPQCardinalityEvaluator(kPathIndex, new QueryResult())
      val sampleEstimation: BigInt = evaluator.calculateCardinality(crpq, Map(currentNodeId -> sampleNodes.toSet))
      val scaleUpFactor = BigDecimal.valueOf(1 / samplingFactor).toBigInt()
      val estimation = sampleEstimation * scaleUpFactor
      (currentNodeId, estimation)
    }

    val numOfNodes = BigDecimal.valueOf(estimationPerNode.map(_._1).size)
    val meanOfAllEstimations = ((estimationPerNode.map(_._2).sum).doubleValue() / numOfNodes).toBigInt()
    meanOfAllEstimations
  }
}
