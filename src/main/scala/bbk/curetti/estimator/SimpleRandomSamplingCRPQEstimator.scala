package bbk.curetti.estimator

import bbk.curetti.evaluator.{CRPQCardinalityEvaluator, QueryResult}
import bbk.curetti.kpathindex.KPathIndexInMemory
import bbk.curetti.kpathtable.KPathTableInMemory
import bbk.curetti.model.query.CRPQ
import bbk.curetti.sampling.NodeRandomSampler

class SimpleRandomSamplingCRPQEstimator(kPathTable: KPathTableInMemory,
  kPathIndex: KPathIndexInMemory,
  samplingFactor: Double)
  extends AbstractCRPQCardinalityEstimator(kPathTable) {

	override def estimateCardinality(crpq: CRPQ): BigInt = {
		val nodeIds = getNodeIdsFromCrpq(crpq)
		val nodeSampleValues = nodeIds.map { nodeId =>
			val incomingPathIds = getIncomingPathIdsFor(nodeId, crpq)
			val outgoingPathIds = getOutgoingPathIdsFor(nodeId, crpq)
			val nodeValues = getNodeValues(incomingPathIds, outgoingPathIds)

			val sampleSize = (samplingFactor * nodeValues.size).toInt
			val sampleNodes = NodeRandomSampler.takeSample(nodeValues.toSeq, sampleSize, 2)
			(nodeId, sampleNodes)
		}

		val restrictedValues = Map[String, Set[Long]]()
		nodeSampleValues.foreach { case (nodeId, sampleNodes) =>
			restrictedValues + ((nodeId -> sampleNodes.toSet))
		}
		// restricted evaluation to use only the sampled values for each node
		val evaluator = new CRPQCardinalityEvaluator(kPathIndex, new QueryResult())
		val sampleEvaluation = evaluator.calculateCardinality(crpq, restrictedValues)
		// scale-up factor
		val scaleUpFactor = BigDecimal.valueOf(1 / (Math.pow(samplingFactor, nodeIds.size))).toBigInt()
		sampleEvaluation * scaleUpFactor
	}
}
