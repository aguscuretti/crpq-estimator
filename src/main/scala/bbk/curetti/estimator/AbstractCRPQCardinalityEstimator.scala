package bbk.curetti.estimator

import bbk.curetti.kpathtable.KPathTableInMemory
import bbk.curetti.model.query.CRPQ
import grizzled.slf4j.Logging

import scala.collection.mutable.ListBuffer


abstract class AbstractCRPQCardinalityEstimator(kPathTable: KPathTableInMemory) extends EstimatorCommon with Logging {

  def estimateCardinality(crpq: CRPQ): BigInt

  private[estimator] def getNodeIdsFromCrpq(crpq: CRPQ): Seq[String] = {
    val nodes = collection.mutable.Set[String]()
    crpq.rpqs.map { rpq =>
      nodes += rpq.source.id
      nodes += rpq.target.id
    }
    nodes.toSeq
  }

  private[estimator] def getPathIdsFromCrpq(crpq: CRPQ): Seq[(Long, String, String)] = {
    val pathIds = collection.mutable.Set[Long]()
    crpq.rpqs.map(path => (path.pathId, path.source.id, path.target.id))
  }

  private[estimator] def getNodeValues(incomingPathIds: Seq[(Long, FromNodeId)],
                                       outgoingPathIds: Seq[(Long, ToNodeId)]): Set[Long] = {
    val incomingPossibleValues = ListBuffer[Set[Long]]()
    val outgoingPossibleValues = ListBuffer[Set[Long]]()

    // get all targets of incoming edges
    incomingPathIds foreach { incomingPathId =>
      val pathId = incomingPathId._1
      incomingPossibleValues += kPathTable.search(pathId).targets
    }

    // get all sources of outgoing edges
    outgoingPathIds foreach { outgoingPathId =>
      val pathId = outgoingPathId._1
      outgoingPossibleValues += kPathTable.search(pathId).sources
    }

    var possibleValueSetsToIntersect = ListBuffer[Set[Long]]()

    if (incomingPossibleValues.size > 0)
      possibleValueSetsToIntersect ++= incomingPossibleValues

    if (outgoingPossibleValues.size > 0)
      possibleValueSetsToIntersect ++= outgoingPossibleValues

    intersectPossibleValuesSets(possibleValueSetsToIntersect)

  }

  // todo: refactor to use foldLeft applying the intersect function on each fold
  private[estimator] def intersectPossibleValuesSets(possibleValues: ListBuffer[Set[Long]]): Set[Long] = {
    if (possibleValues.size > 1) { // intersect all sets
      val head = possibleValues.head
      var intersectionResult = head
      val nextSets = possibleValues -= head
      nextSets foreach{ set =>
        intersectionResult = intersectionResult.intersect(set)
      }
      intersectionResult
    } else  // return the only set of values
      possibleValues.head
  }

  private[estimator] def getOutgoingPathIdsFor(node: String,
                                               crpq: CRPQ,
                                               avoidConnectiosToNodes: Option[List[String]] = None): Seq[(Long, ToNodeId)] = {
    if(avoidConnectiosToNodes.isDefined){
      crpq.rpqs
        .filter(rpq => (rpq.source.id == node && !avoidConnectiosToNodes.get.contains(rpq.target.id)))
        .map(rpq => (rpq.pathId, rpq.target.id)).distinct
    } else {
      crpq.rpqs
        .filter(_.source.id == node)
        .map(rpq => (rpq.pathId, rpq.target.id)).distinct
    }
  }

  private[estimator] def getIncomingPathIdsFor(node: String,
                                               crpq: CRPQ,
                                               avoidConnectiosToNodes: Option[List[String]] = None): Seq[(PathId, FromNodeId)] = {

    if(avoidConnectiosToNodes.isDefined){
      crpq.rpqs
        .filter(rpq => (rpq.target.id == node && !avoidConnectiosToNodes.get.contains(rpq.source.id)))
        .map(rpq => (rpq.pathId, rpq.source.id)).distinct
    } else {
      crpq.rpqs
        .filter(_.target.id == node)
        .map(rpq => (rpq.pathId, rpq.source.id)).distinct
    }

  }

	def updateMap[A, B](map: Map[A, Set[B]], key: A, value: B) =
		map + ((key, map.getOrElse(key, Set()) + value))

}

