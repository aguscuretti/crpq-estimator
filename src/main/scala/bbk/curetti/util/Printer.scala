//package bbk.curetti.util
//
//import java.io.{BufferedWriter, FileWriter}
//
//import au.com.bytecode.opencsv.CSVWriter
//import bbk.curetti.Main.{createGroupedAverageResultPerSf, dbName}
//
//class Printer[T] {
//  def print(rows: Seq[T], ) = {
//    val avgResultsTogetherFile = s"results/${dbName}_avg_results_all_strategies_together.txt"
//    val outWriter = new BufferedWriter(new FileWriter(avgResultsTogetherFile))
//    val csvWriter = new CSVWriter(outWriter)
//
//    rows.foreach{ row =>
//      csvWriter.writeNext(createGroupedAverageResultPerSf(row))
//    }
//  }
//
//
//}
