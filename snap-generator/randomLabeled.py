import snap

# Network = snap.GenRndGnm(snap.PNEANet, 100, 1000)
# snap.SaveEdgeList(Network, "PNEANet.edges")
# LoadedNet = snap.LoadEdgeList(snap.PNEANet, "PNEANet.edges", 0, 1, '\t')
# LoadedNet.Dump()

Graph = snap.GenRndGnm(snap.PNGraph, 10, 1000)
snap.SaveEdgeList(Graph, "PNGraph.edges")
LoadedGraph = snap.LoadEdgeList(snap.PNGraph, "PNGraph.edges", 0, 1, '\t')
LoadedGraph.Dump()