import snap
import sys

# Generates a random Forest Fire, directed graph with given probabilities.

Graph = snap.GenForestFire(2000, 0.35, 0.35)
with open("rdnFF2K.txt", "w") as f:
    for EI in Graph.Edges():
        f.write("%d %d 1\n" % (EI.GetSrcNId(), EI.GetDstNId()))
    