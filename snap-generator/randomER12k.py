import snap
import sys

# Generates an Erdos-Renyi random graph of the specified GraphType.
# GenRndGnm(GraphType, Nodes, Edges, IsDir=True, Rnd=TRnd)
Graph = snap.GenRndGnm(snap.PNGraph, 12333, 147996)
with open("rdnER12K.txt", "w") as f:
    for EI in Graph.Edges():
        f.write("%d %d 1\n" % (EI.GetSrcNId(), EI.GetDstNId()))
    triads = snap.GetTriads(Graph)
    print("\nTriads %d" % triads)
    diam = snap.GetBfsFullDiam(Graph, 10)
    print("\nDiameter %d" % diam)