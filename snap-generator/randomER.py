import snap
import sys

# Generates an Erdos-Renyi random graph of the specified GraphType.
# GenRndGnm(GraphType, Nodes, Edges, IsDir=True, Rnd=TRnd)
Graph = snap.GenRndGnm(snap.PNGraph, 10000, 60000)
with open("rdnER.txt", "w") as f:
    for EI in Graph.Edges():
        f.write("%d %d 1\n" % (EI.GetSrcNId(), EI.GetDstNId()))